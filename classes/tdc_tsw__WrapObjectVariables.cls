/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WrapObjectVariables {
    @AuraEnabled
    global String description;
    @AuraEnabled
    global String duedate;
    @AuraEnabled
    global String enddatetime;
    @AuraEnabled
    global String objectName;
    @AuraEnabled
    global String startdatetime;
    @AuraEnabled
    global String subject;
    global WrapObjectVariables() {

    }
}
