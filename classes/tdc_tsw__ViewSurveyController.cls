/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ViewSurveyController {
    @RemoteAction
    global static String cloneSurvey(String surveyId, String SurveyName, String surveyBriefDes, String surveyKeywords, String activeSurvey, String objectName) {
        return null;
    }
    @RemoteAction
    global static String createAnswer(String questionId, String possibleAnswers, String default1) {
        return null;
    }
    global static String createQuestion(String surveyId, String questionName, String questionText, String objectName, String isFirstQuestion, String answerId) {
        return null;
    }
    @RemoteAction
    global static String createQuestion(String surveyId, String questionName, String questionText, String objectName, String isFirstQuestion, String answerId, String filename, String filedata) {
        return null;
    }
    global static String createSurvey(String SurveyName, String surveyBriefDes, String surveyKeywords, String activeSurvey) {
        return null;
    }
    @RemoteAction
    global static String createSurvey(String SurveyName, String surveyBriefDes, String surveyKeywords, String activeSurvey, String objectName) {
        return null;
    }
    @RemoteAction
    global static String deleteAnswer(String answerId) {
        return null;
    }
    @RemoteAction
    global static String deleteQuestion(String questionId, String surveyId, String isFirstQuestion) {
        return null;
    }
    @RemoteAction
    global static String deleteSurvey(String t) {
        return null;
    }
    @RemoteAction
    global static String editAnswer(String answerId, String possibleAnswers, String default1) {
        return null;
    }
    @RemoteAction
    global static String editAnswerLookup(String answerId, String nextQuestionId) {
        return null;
    }
    global static String editQuestion(String surveyId, String questionName, String questionText, String objectName, String isFirstQuestion, String questionId) {
        return null;
    }
    @RemoteAction
    global static String editQuestion(String surveyId, String questionName, String questionText, String objectName, String isFirstQuestion, String questionId, String filename, String filedata, String oldAttachementID) {
        return null;
    }
    global static String editSurvey(String surveyId, String SurveyName, String surveyBriefDes, String surveyKeywords, String activeSurvey) {
        return null;
    }
    @RemoteAction
    global static String editSurvey(String surveyId, String SurveyName, String surveyBriefDes, String surveyKeywords, String activeSurvey, String objectName) {
        return null;
    }
    @RemoteAction
    global static String editSurveyStatus(String surveyId, String activeSurvey) {
        return null;
    }
    @RemoteAction
    global static String getFields(String selectedObject) {
        return null;
    }
}
