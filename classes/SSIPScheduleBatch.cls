global class SSIPScheduleBatch implements Database.Batchable<Sobject>, Database.RaisesPlatformEvents{
    private static final Date dt = Date.today();
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Account__c, Country__c, IsDeleted, Name, Payment_Term__c, Schedule_Date__c, Shipping_Address__c, Billing_Address__c, Shipping_Method__c, SSIP_Rule__c, Status__c, SSIP_Rule__r.Price_Book__c,';
               query+= 'SSIP_Rule__r.Product__c, SSIP_Rule__r.First_Order__c , SSIP_Rule__r.Quantity__c, Rescheduled_Shipment_Date__c ';
               query+= 'FROM SSIP_Schedule__c WHERE (Schedule_Date__c =: dt OR Rescheduled_Shipment_Date__c =: dt) AND Status__c = \'Open\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SSIP_Schedule__c> scope){
        Set<Id> setAccountId = new Set<Id>();
        for(SSIP_Schedule__c ssipSchedule : scope) setAccountId.add(ssipSchedule.Account__c);
        Map<String, Order> mapOrder = new Map<String, Order>();
        Map<String, List<OrderItem>> mapOrderItems = new Map<String, List<OrderItem>>();
        Map<String, PricebookEntry> mapPriceBookEntry = new Map<String, PricebookEntry>();
        Map<String, List<SSIP_Schedule__c>> mapSSIPSchedule = new Map<String, List<SSIP_Schedule__c>>();
        List<OrderItem> lstOrderItem = new List<OrderItem>();
        List<SSIP_Schedule__c> lstSSIPSchedule = new List<SSIP_Schedule__c>();
        Set<Id> setProductId = new Set<Id>();
        Set<Id> setPriceBookId = new Set<Id>();
        Set<Id> setParentOrderId = new Set<Id>();
        String query = 'SELECT Account__c, Country__c, IsDeleted, Name, Payment_Term__c, Schedule_Date__c, Shipping_Address__c, Billing_Address__c, Shipping_Method__c, SSIP_Rule__c, Status__c, SSIP_Rule__r.Price_Book__c,';
               query+= 'SSIP_Rule__r.Product__c, SSIP_Rule__r.First_Order__c , SSIP_Rule__r.Quantity__c, Rescheduled_Shipment_Date__c ';
               query+= 'FROM SSIP_Schedule__c WHERE (Schedule_Date__c =: dt OR Rescheduled_Shipment_Date__c =: dt) AND Status__c = \'Open\' AND Account__c IN: setAccountId';
        
        List<SSIP_Schedule__c> lstNewSSIPSchedule = Database.query(query);
        for(SSIP_Schedule__c ssipSchedule : lstNewSSIPSchedule){
            if(ssipSchedule.Rescheduled_Shipment_Date__c != null && ssipSchedule.Rescheduled_Shipment_Date__c != Date.today()) continue;
            setProductId.add(ssipSchedule.SSIP_Rule__r.Product__c);
            setPriceBookId.add(ssipSchedule.SSIP_Rule__r.Price_Book__c);
            setAccountId.add(ssipSchedule.Account__c);
            setParentOrderId.add(ssipSchedule.SSIP_Rule__r.First_Order__c);
        }
        
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Name, RecordType.DeveloperName, (SELECT Id, Type__c FROM Addresses__r WHERE Primary_Flag__c = true LIMIT 2), (SELECT Id FROM Finance_Details__r WHERE Primary__c = true AND RecordType.Name = 'Credit Card' LIMIT 1) FROM Account WHERE Id IN: setAccountId]);
        Map<Id, Order> mapFirstOrder = new Map<Id, Order>([SELECT Id, RecordTypeId FROM Order WHERE Id IN: setParentOrderId]);
        for(PricebookEntry pbe : [SELECT Discount_Amount__c,Pricebook2Id,Product2Id,Split_Price__c,Split_Qty__c,SSIP_Next_Order_In_Days__c,SSIP_Periodic_Order_Quantity__c,UnitPrice FROM PricebookEntry WHERE Pricebook2Id IN: setPriceBookId AND Product2Id IN: setProductId]) mapPriceBookEntry.put(pbe.Pricebook2Id +'_'+ pbe.Product2Id, pbe);  
        for(SSIP_Schedule__c ssipSchedule : lstNewSSIPSchedule){
            if(ssipSchedule.Rescheduled_Shipment_Date__c != null && ssipSchedule.Rescheduled_Shipment_Date__c != Date.today()) continue;
            String uniqueKey = ssipSchedule.Account__c +'_'+ ssipSchedule.SSIP_Rule__r.Price_Book__c;
            if(!mapSSIPSchedule.containsKey(uniqueKey)) mapSSIPSchedule.put(uniqueKey, new List<SSIP_Schedule__c>());
            mapSSIPSchedule.get(uniqueKey).add(ssipSchedule);
            String shipToAddress = mapAccount.get(ssipSchedule.Account__c).Addresses__r.size() >= 1 && mapAccount.get(ssipSchedule.Account__c).Addresses__r[0].Type__c == 'Ship To' ? mapAccount.get(ssipSchedule.Account__c).Addresses__r[0].Id : 
                                   mapAccount.get(ssipSchedule.Account__c).Addresses__r.size() == 2 && mapAccount.get(ssipSchedule.Account__c).Addresses__r[1].Type__c == 'Ship To' ? mapAccount.get(ssipSchedule.Account__c).Addresses__r[1].Id : null;
                                   
            String billToAddress = mapAccount.get(ssipSchedule.Account__c).Addresses__r.size() >= 1 && mapAccount.get(ssipSchedule.Account__c).Addresses__r[0].Type__c == 'Bill To' ? mapAccount.get(ssipSchedule.Account__c).Addresses__r[0].Id : 
                                   mapAccount.get(ssipSchedule.Account__c).Addresses__r.size() == 2 && mapAccount.get(ssipSchedule.Account__c).Addresses__r[1].Type__c == 'Bill To' ? mapAccount.get(ssipSchedule.Account__c).Addresses__r[1].Id : null;
                                   
            String financialDetail =  mapAccount.get(ssipSchedule.Account__c).Finance_Details__r.isEmpty() ? null :  mapAccount.get(ssipSchedule.Account__c).Finance_Details__r[0].Id;
            System.debug('======financialDetail====='+financialDetail);
            System.debug('======Payment_Term__c====='+ssipSchedule.Payment_Term__c);
            if((shipToAddress == null && ssipSchedule.Shipping_Address__c == null) || (billToAddress == null && ssipSchedule.Billing_Address__c == null) || (financialDetail == null && ssipSchedule.Payment_Term__c == 'net0')){
                String error = (shipToAddress == '' && ssipSchedule.Shipping_Address__c == null) ? 'Shipping Address is missing.' : '';
                       error+= (billToAddress == '' && ssipSchedule.Billing_Address__c == null)  ? ' Billing Address is missing.' : '';
                       error+= financialDetail == '' ? 'Financial Detail is missing!' : '';
                ErrorUtility.LogError('Consume', 'SSIPScheduleBatch', ssipSchedule.Id, 'SSIPScheduleBatch', error, '', '', 'Informational');
                continue;
            }
            
            mapOrder.put(uniqueKey, orderMapping(mapAccount.get(ssipSchedule.Account__c), mapFirstOrder.get(ssipSchedule.SSIP_Rule__r.First_Order__c), ssipSchedule.SSIP_Rule__r.Price_Book__c, ssipSchedule, null, shipToAddress, billToAddress, financialDetail));
            if(!mapOrderItems.containsKey(uniqueKey)) mapOrderItems.put(uniqueKey, new List<OrderItem>());
            mapOrderItems.get(uniqueKey).add(orderItemMapping(mapPriceBookEntry.get(ssipSchedule.SSIP_Rule__r.Price_Book__c +'_'+ ssipSchedule.SSIP_Rule__r.Product__c), ssipSchedule));
        }
        System.debug('====mapOrder====='+mapOrder);
        List<String> lstUniqueKey = new List<String>();
        List<Order> lstOrder = new List<Order>();
        for(String uniqueKey : mapOrder.keySet()){
            lstUniqueKey.add(uniqueKey);
            lstOrder.add(mapOrder.get(uniqueKey));
        }
        
        Database.SaveResult[] srList = Database.insert(lstOrder, false);
        for(Integer i = 0; i < srList.size(); i++){
            String uniqueKey = lstUniqueKey[i];
            if(srList[i].isSuccess()){
                for(OrderItem oi : mapOrderItems.get(uniqueKey)){
                    oi.OrderId = lstOrder[i].Id;
                    lstOrderItem.add(oi);
                }
            }else {
                String error = '';
                for(Database.Error err : srList[i].getErrors()) {
                    error += error == '' ? 'Order ' + err.getMessage() : ', ' + err.getMessage();
                }
                ErrorUtility.LogError('Consume', 'SSIPScheduleBatch', '', 'SSIPScheduleBatch', error, '', '', 'Informational');
                System.debug('====error in order====='+error);
            }
        }
        Map<Id, Order> mapOrderNew = new Map<Id, Order>(lstOrder);
        List<Order> lstOrderToDelete = new List<Order>();
        srList = Database.insert(lstOrderItem, false);
        for(Integer i = 0; i < srList.size(); i++){
            if(srList[i].isSuccess()){
                
            }else {
                if(mapOrderNew.containsKey(lstOrderItem[i].OrderId)){
                    lstOrderToDelete.add(mapOrderNew.remove(lstOrderItem[i].OrderId));
                }
                String error = '';
                for(Database.Error err : srList[i].getErrors()) {
                    error += error == '' ? 'Order ' + err.getMessage() : ', ' + err.getMessage();
                }
                System.debug('====error in orderitem====='+error);
            }
        }
        
        
        for(Integer i = 0; i < lstOrder.size(); i++){
            String uniqueKey = lstUniqueKey[i];
            if(!mapOrderNew.containsKey(lstOrder[i].Id)) continue;
            for(SSIP_Schedule__c ssipSchedule : mapSSIPSchedule.get(uniqueKey)){
                ssipSchedule.Order__c = lstOrder[i].Id;
                ssipSchedule.Status__c = 'In Progress';
                lstSSIPSchedule.add(ssipSchedule);
            }
        }
        
        update lstSSIPSchedule;
        delete lstOrderToDelete;
        
        for(Order objOrder : [SELECT Id, Account.RecordType.Name, Payment_Terms__c, Finance_Detail__c FROM Order WHERE Id IN: lstOrder]){
            System.debug('===121====objOrder====='+objOrder);
            System.enqueueJob(new SSIPScheduleJobUtiltiy.CalculateTaxQueueable(objOrder.Id, objOrder.Payment_Terms__c, objOrder.Account.RecordType.Name.substring(0,2)));
        }
        //Need to logic for to handle the error using platform events..
    }
    
    private Order orderMapping(Account objAccount, Order firstOrder, String priceBookId, SSIP_Schedule__c ssipSchedule, String recordTypeId, String shippingAddressId, String billingAddressId, String financialDetailId){
        return new Order(AccountId = objAccount.Id, RecordTypeId = firstOrder.RecordTypeId, Customer_Ship_To_Address__c = ssipSchedule.Shipping_Address__c != null ? ssipSchedule.Shipping_Address__c : shippingAddressId, 
                         Pricebook2Id = priceBookId, Type = objAccount.RecordType.DeveloperName.substring(0, 2) + ' STANDARD', 
                         EffectiveDate = Date.valueOf(System.now().format('YYYY-MM-dd', 'America/Los_Angeles')), Status = 'Draft', 
                         Customer_Bill_To_Address__c = ssipSchedule.Billing_Address__c != null ? ssipSchedule.Billing_Address__c : billingAddressId, 
                         Finance_Detail__c = ssipSchedule.Payment_Term__c == 'net0' ? financialDetailId : null, Payment_Terms__c = ssipSchedule.Payment_Term__c);
    }
    
    private OrderItem orderItemMapping(PricebookEntry pbe, SSIP_Schedule__c ssipSchedule){
        return new OrderItem(PricebookEntryId = pbe.Id, UnitPrice = pbe.UnitPrice, Quantity = ssipSchedule.SSIP_Rule__r.Quantity__c);
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}