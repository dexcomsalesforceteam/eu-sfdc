public virtual class PEClsOrderEventUtility {
    public static String version = '1.0'; 
    public static final Map<String, String> mapRecordTypeNameToID;
    public static final Map<String, String> mapAddressRecordTypeNameToID;
    public static final Map<String, Order_Event_Setting__mdt> mapOrderEventSetting;
    public static final Map<String, String> mapCountry;
    public static final Map<String, String> mapState;
    public static final Map<String, String> mapOrderTypeENUM;
    static{
        mapCountry = PEClsOrderEventUtility.getPicklistValues('Account', 'ShippingCountryCode');
        mapState = PEClsOrderEventUtility.getPicklistValues('Account', 'ShippingStateCode');
        mapRecordTypeNameToID = new Map<String, String>();
        mapAddressRecordTypeNameToID = new Map<String, String>();
        mapOrderTypeENUM = new Map<String, String>();
        mapOrderEventSetting = new Map<String, Order_Event_Setting__mdt>();
        for(Schema.RecordTypeInfo rtInfo : Schema.SObjectType.Order.getRecordTypeInfosByName().values()){
            mapRecordTypeNameToID.put(rtInfo.getName().toLowerCase(), rtInfo.getRecordTypeId());
        }
        for(Schema.RecordTypeInfo rtInfo : Schema.SObjectType.Address__c.getRecordTypeInfosByName().values()){
            mapAddressRecordTypeNameToID.put(rtInfo.getDeveloperName().toLowerCase(), rtInfo.getRecordTypeId());
        }
        for(Order_Event_Setting__mdt oEventSetting : [SELECT Id, DeveloperName, MasterLabel, SFDC_ORDER_TYPE__c, SFDC_RECORD_TYPE__c, EBS_ORDER_TYPE__c, FUSION_ORDER_TYPE__c FROM Order_Event_Setting__mdt]){
            mapOrderEventSetting.put(oEventSetting.DeveloperName , oEventSetting);
            mapOrderTypeENUM.put(oEventSetting.SFDC_ORDER_TYPE__c.toUpperCase(), oEventSetting.DeveloperName);
        } 
    }
    public class OrderLineItem{
        public String itemID;
        public String sku;
        public Decimal quantity;
        public Decimal price;
        public Boolean isBundle;
        public Boolean isBundleItem;
        public String skuBundle;
        public String url; 
        
        public OrderLineItem(String sku, Decimal quantity, Decimal price){
            this.sku =  sku;
            this.quantity = quantity;
            this.price = price;
        }
        public OrderLineItem(OrderItem oli){
            this.itemID = oli.Id;
            this.sku = oli.Product2.Name;
            this.quantity = oli.Quantity;
            this.price = oli.UnitPrice;
            this.isBundle = oli.isBundle__c;
            this.isBundleItem = oli.isBundleItem__c;
            this.skuBundle = oli.SkuBundle__r.Name;
            this.url = '';
        }
        
    }
    public static CRM_Order_Event_Out__e mappingOrderEvent(Order objOrder, List<OrderLineItem> lstOLI, CRM_Order_Event__e oEvent, String stage, String action){
        return new CRM_Order_Event_Out__e(version__c = version, stage__c = stage, accountId__c = objOrder.AccountId, eventDate__c = String.valueOf(objOrder.LastModifiedDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ')), invoiceDate__c = String.valueOf(objOrder.Invoice_Date__c), invoiceNumber__c = objOrder.Invoice_Number__c, warehouse__c = objOrder.Warehouse__c,pricebookName__c = objOrder.PriceBook2.Name,
                                          operation__c = oEvent != null && oEvent.operation__c != 'null'? oEvent.operation__c : action != null ? action : 'update', systemOfOrigin__c = objOrder.System_Of_Origin__c,systemOfOriginID__c = objOrder.System_Of_Origin_ID__c, billingAddressBusinessName__c = objOrder.Customer_Bill_To_Address__r.Business_Name__c, billingAddressId__c = objOrder.Customer_Bill_To_Address__c, billingAddress1__c = objOrder.Customer_Bill_To_Address__r.Address_Line_1__c,
                                          billingAddress2__c = objOrder.Customer_Bill_To_Address__r.Address_Line_2__c, billingAddress3__c = objOrder.Customer_Bill_To_Address__r.Address_Line_3__c, billingAddressCity__c = objOrder.Customer_Bill_To_Address__r.City__c, billingAddressCountry__c = objOrder.Customer_Bill_To_Address__r.Country__c, billingAddressFirstName__c = objOrder.Customer_Bill_To_Address__r.First_Name__c, 
                                          billingAddressLastName__c = objOrder.Customer_Bill_To_Address__r.Last_Name__c, billingAddressPostalCode__c = objOrder.Customer_Bill_To_Address__r.Postal_Code__c, billingAddressIsLocked__c = objOrder.Customer_Bill_To_Address__r.isLocked__c, billingAddressPrimaryPhone__c = objOrder.Customer_Bill_To_Address__r.Primary_Phone__c, 
                                          billingAddressStateProvince__c = objOrder.Customer_Bill_To_Address__r.State__c, shippingAddressBusinessName__c = objOrder.Customer_Ship_To_Address__r.Business_Name__c, shippingAddress1__c = objOrder.Customer_Ship_To_Address__r.Address_Line_1__c, shippingAddress2__c = objOrder.Customer_Ship_To_Address__r.Address_Line_2__c, 
                                          shippingAddress3__c = objOrder.Customer_Ship_To_Address__r.Address_Line_3__c, shippingAddressCity__c = objOrder.Customer_Ship_To_Address__r.City__c, shippingAddressCountry__c = objOrder.Customer_Ship_To_Address__r.Country__c, shippingAddressFirstName__c = objOrder.Customer_Ship_To_Address__r.First_Name__c, shippingAddressId__c = objOrder.Customer_Ship_To_Address__c,
                                          shippingAddressLastName__c = objOrder.Customer_Ship_To_Address__r.Last_Name__c, shippingAddressPostalCode__c = objOrder.Customer_Ship_To_Address__r.Postal_Code__c, shippingAddressIsLocked__c = objOrder.Customer_Ship_To_Address__r.isLocked__c, shippingAddressPrimaryPhone__c = objOrder.Customer_Ship_To_Address__r.Primary_Phone__c, 
                                          shippingAddressStateProvince__c = objOrder.Customer_Ship_To_Address__r.State__c, Line_Items__c = JSON.serialize(lstOLI),  creditCardAuthorizationCode__c = objOrder.Credit_Card_Authorization_Code__c, creditCardToken__c = objOrder.Finance_Detail__r.Token__c, creditCardType__c = objOrder.Finance_Detail__r.Card_Type__c, 
                                          creditCardExpirationDate__c = objOrder.Finance_Detail__r.Expiry_Date__c, orderNumber__c = objOrder.OrderNumber, creditCardId__c = objOrder.Finance_Detail__c,
                                          isSignatureRequired__c = objOrder.Signature_Required__c, shippingMethod__c = objOrder.Shipping_Method__c, billingAddressAccountID__c = objOrder.AccountId,
                                          billingAddressCountryCode__c = mapCountry.containsKey(objOrder.Customer_Bill_To_Address__r.Country__c) ? mapCountry.get(objOrder.Customer_Bill_To_Address__r.Country__c) : 'XX',
                                          shippingAddressCountryCode__c = mapCountry.containsKey(objOrder.Customer_Ship_To_Address__r.Country__c) ? mapCountry.get(objOrder.Customer_Ship_To_Address__r.Country__c) : 'XX',
                                          shippingAddressAccountID__c = objOrder.AccountId, eventSource__c = 'crm', entityType__c = 'order', eventSourceID__c = objOrder.Id,
                                          totalAmount__c = objOrder.TotalAmount, status__c = objOrder.Status.toLowerCase(), orderType__c = PEClsOrderEventUtility.parseOrderType(objOrder.Type, objOrder.System_Of_Origin__c));
    }
    
    public static CRM_OrderItem_Event_Out_V1__e mappingOrderEventV11(Order objOrder, List<OrderLineItem> lstOLI, CRM_Order_Event__e oEvent, String stage, String action){
        return new CRM_OrderItem_Event_Out_V1__e(/*version__c = version, stage__c = stage, accountId__c = objOrder.AccountId, eventDate__c = String.valueOf(objOrder.LastModifiedDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ')), invoiceDate__c = String.valueOf(objOrder.Invoice_Date__c), invoiceNumber__c = objOrder.Invoice_Number__c, warehouse__c = objOrder.Warehouse__c,pricebookName__c = objOrder.PriceBook2.Name,
                                                 operation__c = oEvent != null && oEvent.operation__c != 'null'? oEvent.operation__c : action != null ? action : 'update', systemOfOrigin__c = objOrder.System_Of_Origin__c,systemOfOriginID__c = objOrder.System_Of_Origin_ID__c, billingAddressBusinessName__c = objOrder.Customer_Bill_To_Address__r.Business_Name__c, billingAddressId__c = objOrder.Customer_Bill_To_Address__c, billingAddress1__c = objOrder.Customer_Bill_To_Address__r.Address_Line_1__c,
                                                 billingAddress2__c = objOrder.Customer_Bill_To_Address__r.Address_Line_2__c, billingAddress3__c = objOrder.Customer_Bill_To_Address__r.Address_Line_3__c, billingAddressCity__c = objOrder.Customer_Bill_To_Address__r.City__c, billingAddressCountry__c = objOrder.Customer_Bill_To_Address__r.Country__c, billingAddressFirstName__c = objOrder.Customer_Bill_To_Address__r.First_Name__c, 
                                                 billingAddressLastName__c = objOrder.Customer_Bill_To_Address__r.Last_Name__c, billingAddressPostalCode__c = objOrder.Customer_Bill_To_Address__r.Postal_Code__c, billingAddressIsLocked__c = objOrder.Customer_Bill_To_Address__r.isLocked__c, billingAddressPrimaryPhone__c = objOrder.Customer_Bill_To_Address__r.Primary_Phone__c, 
                                                 billingAddressStateProvince__c = objOrder.Customer_Bill_To_Address__r.State__c, shippingAddressBusinessName__c = objOrder.Customer_Ship_To_Address__r.Business_Name__c, shippingAddress1__c = objOrder.Customer_Ship_To_Address__r.Address_Line_1__c, shippingAddress2__c = objOrder.Customer_Ship_To_Address__r.Address_Line_2__c, 
                                                 shippingAddress3__c = objOrder.Customer_Ship_To_Address__r.Address_Line_3__c, shippingAddressCity__c = objOrder.Customer_Ship_To_Address__r.City__c, shippingAddressCountry__c = objOrder.Customer_Ship_To_Address__r.Country__c, shippingAddressFirstName__c = objOrder.Customer_Ship_To_Address__r.First_Name__c, shippingAddressId__c = objOrder.Customer_Ship_To_Address__c,
                                                 shippingAddressLastName__c = objOrder.Customer_Ship_To_Address__r.Last_Name__c, shippingAddressPostalCode__c = objOrder.Customer_Ship_To_Address__r.Postal_Code__c, shippingAddressIsLocked__c = objOrder.Customer_Ship_To_Address__r.isLocked__c, shippingAddressPrimaryPhone__c = objOrder.Customer_Ship_To_Address__r.Primary_Phone__c, 
                                                 shippingAddressStateProvince__c = objOrder.Customer_Ship_To_Address__r.State__c, Line_Items__c = JSON.serialize(lstOLI),  creditCardAuthorizationCode__c = objOrder.Credit_Card_Authorization_Code__c, creditCardToken__c = objOrder.Finance_Detail__r.Token__c, creditCardType__c = objOrder.Finance_Detail__r.Card_Type__c, 
                                                 creditCardExpirationDate__c = objOrder.Finance_Detail__r.Expiry_Date__c, orderNumber__c = objOrder.OrderNumber, creditCardId__c = objOrder.Finance_Detail__c,
                                                 isSignatureRequired__c = objOrder.Signature_Required__c, shippingMethod__c = objOrder.Shipping_Method__c, billingAddressAccountID__c = objOrder.AccountId,
                                                 billingAddressCountryCode__c = mapCountry.containsKey(objOrder.Customer_Bill_To_Address__r.Country__c) ? mapCountry.get(objOrder.Customer_Bill_To_Address__r.Country__c) : 'XX',
                                                 shippingAddressCountryCode__c = mapCountry.containsKey(objOrder.Customer_Ship_To_Address__r.Country__c) ? mapCountry.get(objOrder.Customer_Ship_To_Address__r.Country__c) : 'XX',
                                                 shippingAddressAccountID__c = objOrder.AccountId, eventSource__c = 'crm', entityType__c = 'order', eventSourceID__c = objOrder.Id,
                                                 totalAmount__c = objOrder.TotalAmount, status__c = objOrder.Status.toLowerCase(), orderType__c = PEClsOrderEventUtility.parseOrderType(objOrder.Type, objOrder.System_Of_Origin__c)*/);
    }
    
    public static String parseOrderType(String orderType, String origin){
        if(String.isEmpty(orderType) || String.isBlank(orderType)) return orderType;
        List<String> lstOrderType = orderType.split(' ');
        String countryCode = lstOrderType.remove(0);
        String newOrderType = String.join(lstOrderType, ' ').toUpperCase().trim();
        if(mapOrderTypeENUM.containsKey(newOrderType)){
            return origin.containsIgnoreCase('ECOM') && mapOrderEventSetting.get(mapOrderTypeENUM.get(newOrderType)).EBS_ORDER_TYPE__c != null ? countryCode +' '+ mapOrderEventSetting.get(mapOrderTypeENUM.get(newOrderType)).EBS_ORDER_TYPE__c : 
                   origin.containsIgnoreCase('CRM') && mapOrderEventSetting.get(mapOrderTypeENUM.get(newOrderType)).FUSION_ORDER_TYPE__c != null ? countryCode +' '+ mapOrderEventSetting.get(mapOrderTypeENUM.get(newOrderType)).FUSION_ORDER_TYPE__c : orderType;
        }
        return orderType;
    }
    
    public static List<Order> getOrders(Set<String> setOrderId, Set<String> setOrderNumber, Set<String> setAccountID){
        return [SELECT Id, AccountId, LastModifiedDate, Invoice_Date__c, Invoice_Number__c, Type, Warehouse__c, PriceBook2.Name, System_Of_Origin__c, System_Of_Origin_ID__c, Customer_Bill_To_Address__r.Business_Name__c, Customer_Bill_To_Address__r.Address_Line_1__c, Customer_Bill_To_Address__r.Address_Line_2__c,
                       Customer_Bill_To_Address__r.Address_Line_3__c, Customer_Bill_To_Address__r.City__c, Customer_Bill_To_Address__r.Country__c, Customer_Bill_To_Address__r.First_Name__c, Customer_Bill_To_Address__r.Last_Name__c, Customer_Bill_To_Address__r.Postal_Code__c,Customer_Bill_To_Address__c,
                       Customer_Bill_To_Address__r.isLocked__c, Customer_Bill_To_Address__r.Primary_Phone__c,Customer_Bill_To_Address__r.State__c, Customer_Ship_To_Address__r.Business_Name__c, Customer_Ship_To_Address__r.Address_Line_1__c,Customer_Ship_To_Address__r.State__c,Customer_Ship_To_Address__c,
                       Customer_Ship_To_Address__r.Address_Line_2__c, Customer_Ship_To_Address__r.Address_Line_3__c, Customer_Ship_To_Address__r.City__c, Customer_Ship_To_Address__r.Country__c, Customer_Ship_To_Address__r.First_Name__c, Customer_Ship_To_Address__r.Last_Name__c,
                       Customer_Ship_To_Address__r.Postal_Code__c, Customer_Ship_To_Address__r.isLocked__c, Customer_Ship_To_Address__r.Primary_Phone__c, Credit_Card_Authorization_Code__c,  Finance_Detail__c, Finance_Detail__r.Token__c, Finance_Detail__r.Card_Type__c, Finance_Detail__r.Expiry_Date__c,
                       SOS_Unique_ID__c, Status, OrderNumber, RecordTypeId, OwnerId, Signature_Required__c, Shipping_Method__c,TotalAmount, Account.Sync_Status__c,
                       (SELECT Id, Product2Id, Product2.Name, Quantity, UnitPrice, isBundle__c, isBundleItem__c, SkuBundle__c, SkuBundle__r.Name FROM OrderItems)
                  FROM Order WHERE (Id IN: setOrderId OR OrderNumber IN: setOrderNumber OR AccountID IN: setAccountID)];
    }
    public static String createUniqueKey(Address__c address){
        return parseValue(address.Business_Name__c) +'_'+ parseValue(address.First_Name__c)+'_'+ parseValue(address.Last_Name__c)+'_'+ 
               parseValue(address.Address_Line_1__c)+'_'+ parseValue(address.Address_Line_2__c)+'_'+ parseValue(address.Address_Line_3__c)+'_'+ 
               parseValue(address.City__c)+'_'+ parseValue(address.State__c)+'_'+ parseValue(address.Postal_Code__c)+'_'+ 
               parseValue(address.Country__c)+'_'+ parseValue(address.Type__c)+'_'+ parseValue(address.Account__c)+'_'+ parseValue(address.Primary_Phone__c);
    }
    
    public static String parseValue(String value){
        return value == null || value == 'null' ? '' : value.replaceAll( '\\s+', '');
    }
    public static Map<String, String> getPicklistValues(String objectAPI,String fieldAPI){ 
        Map<String, String> mapPicklist = new Map<String, String>();
        try{
            for(Schema.PicklistEntry sObjPickListEntry : Schema.getGlobalDescribe().get(objectAPI).newSObject().getSObjectType().getDescribe().fields.getMap().get(fieldAPI).getDescribe().getPickListValues()){
                mapPicklist.put(sObjPickListEntry.getValue(), sObjPickListEntry.getLabel());
                mapPicklist.put(sObjPickListEntry.getLabel(), sObjPickListEntry.getValue());
            }
        }catch(Exception ex){
            ErrorUtility.LogError('Consume', 'PEClsHandleOrderEvents.getPicklistValues', 'Error while getting the Country and State Piclist information: '+ex.getMessage(), 'order', null, 'Incorrect data.', '', 'Informational');
        }
        return mapPicklist;
    }
}