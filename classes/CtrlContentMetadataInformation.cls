public class CtrlContentMetadataInformation {
    @AuraEnabled
    //Method will retrieve the contentmetadata for the documents attached to a particular salesforce record
    public static List<Object> getContentMetadata  (String recordId) {
        //Initialize variables
        List<Object> accountList = [Select Id, Name, Content_Document_Id__c, Linked_Objects__c, Document_Type__c, Owned_By_User__c, Document_Created_By_User__c, Document_Created_Date__c, Content_Size__c from Content_Metadata__c Where Id = :recordId OR Account__c = :recordId OR Opportunity__c = :recordId OR Order__c = :recordId OR Opportunity_Account__c = :recordId OR Order_Account__c = :recordId Order by Document_Created_Date__c desc];
        return accountList;
    }
}