public class ClsPlatformEventErrorLogHandler {
    public static void handleInsert(List<Platform_Event_Error_Log__c> errors) {
        List<CRM_Error_Event__e> errEventsList = new List<CRM_Error_Event__e>();
        CRM_Error_Event__e  event;
        string str;
        for(Platform_Event_Error_Log__c error : errors) {
            if (error.Severity__c == 'Success Log' || error.Action__c == 'Error-in') continue;    // Don't publish these events as it is not an error.
            event = new CRM_Error_Event__e();
            event.code__c = '400';
            event.eventDate__c = String.valueOf(error.LastModifiedDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
            event.eventSource__c = error.EventSource__c ;
            event.entityType__c= error.Event__c;
            //event.keyName__c = 'systemOfOriginID';
            event.key__c = error.EventSource__c;
            str = error.Error_Message__c + ' ' + error.Exception_Error__c;  
            event.message__c = str.left(255);
            event.payload__c = error.Event_String__c;
            errEventsList.add(event);
        }
        if (errEventsList.size() > 0) {
            List<Database.SaveResult> svrs = EventBus.publish(errEventsList);
            for (Database.SaveResult svr : svrs) {
                if (!svr.isSuccess()) {
                  system.debug('Error in publishing Account Event. Error: ' + svr.getErrors());
                }
            }
        }
    }
    public static void handleInboundErrors(List<CRM_Error_Event_In__e> errors) {
        List<Platform_Event_Error_Log__c> pees = new List<Platform_Event_Error_Log__c>();
        Platform_Event_Error_Log__c pee;
        for (CRM_Error_Event_In__e inev : errors) {
            pee = new Platform_Event_Error_Log__c();
            pee.Event__c = inev.entityType__c.left(40);   // Label Object
            pee.EventSource__c = inev.key__c.left(40);    // Label EventSource
            pee.Error_Message__c = inev.message__c.left(255);  // Label Message 
            pee.Action__c = 'Error-In';  // Label Action
            pee.Error_Handling_Status__c = 'New';  // Not displayed
            pee.Event_String__c = inev.payload__c;
            pees.add(pee);
        }
        insert pees;
    }
}