/*******************************************************************************************************************
@Description    : OrderItem trigger handler to process orderitem updates
********************************************************************************************************************/   
public class ClsOrderItemTriggerHandler{
    
    //Map, which will be used for updates on Order object
    static Map<Id, Order> orderIdToOrderMap = new Map<Id, Order>();
    static Map<Id, OrderItem> orderItemIdToOrderItemMap = new Map<Id, OrderItem>();
    static boolean isExecuting = false;

    // This Method Updates Related Audit Records If any Changes are made to OrderItems                
    public static void UpdateAuditTrackingonOrder(map<Id,OrderItem> OrderItems, String recordTypeId){
		Set<Id> orderIds = new set<Id>();
        List<Audit_Tracker__c> lstUpdOrderAudits = new List<Audit_Tracker__c>();
		for(OrderItem oi : [SELECT Id,OrderId FROM OrderItem WHERE Id IN :OrderItems.keyset() AND Order.Shipping_Hold_status__c = 'In Progress']) orderIds.add(oi.Orderid);
		if(!orderIds.isEmpty()){
			//Look out for all audit trackers which have Product as verified and then we have to un-verify them
			for(Audit_Tracker__c at: [SELECT Id, Audit_Field_Name__c, Field_Verified__c FROM Audit_Tracker__c WHERE  Object_Id__c in : orderIds AND Audit_Field_Name__c = 'Products'AND Field_Verified__c = true]){
				at.Field_Verified__c = false;
				lstUpdOrderAudits.add(at);
			}
		}
		if(!lstUpdOrderAudits.isEmpty()) update lstUpdOrderAudits;  	
    }
    //Method will group the Order by Id and updates the tracking number
    public static void UpdateTrackingNumberOnOrder (Map<Id, OrderItem> newTriggerOrderItems, Map<Id, OrderItem> oldTriggerOrderItems)
    {
        // Avoid Recursion - If this logic was executed during the same context  
        if(ClsOrderTriggerStaticClass.isExecuting)
            return;
        ClsOrderTriggerStaticClass.isExecuting = true;
        
        //Loop through each order items and find the ones where there was a change in Tracking number and then group the orders to do a final update
        for(OrderItem newOrderItem : newTriggerOrderItems.values())
        {
            Id orderId = newOrderItem.OrderId;
            Order parentOrder = new Order(id=orderId);
            OrderItem oldOrderItem = oldTriggerOrderItems.get(newOrderItem.Id);
            if(oldOrderItem.Tracking_Number__c != newOrderItem.Tracking_Number__c && newOrderItem.Is_Virtual_Product__c == false)
            {
                system.debug('*******************Tracking number time');
                parentOrder.Tracking_Number__c = newOrderItem.Tracking_Number__c;
                orderIdToOrderMap.put(orderId, parentOrder);
            }
            if(oldOrderItem.Serial_Number__c != newOrderItem.Serial_Number__c && newOrderItem.Product_Name__c != null && newOrderItem.Product_Name__c.contains('STK') )
           {
                system.debug('*******************Serial number time');
                parentOrder.Serial_Number__c = newOrderItem.Serial_Number__c;
                if(newOrderItem.Serial_Number__c != null)
                {
                
                    orderIdToOrderMap.put(orderId, parentOrder);
            
                }
            }    
        }
        //Update virtual SKUs if any exist for the Orders
        for(OrderItem virtualItem : [SELECT Id, OrderId, Tracking_Number__c FROM OrderItem WHERE OrderId IN : orderIdToOrderMap.keySet() AND Is_Virtual_Product__c = True])
        {
            virtualItem.Tracking_Number__c = orderIdToOrderMap.get(virtualItem.OrderId).Tracking_Number__c;
            orderItemIdToOrderItemMap.put(virtualItem.Id, virtualItem);
            system.debug('virutalItem Id is ' + virtualItem.Id);
        }
        //Update the Virtual order lines
        system.debug('Entering process to update the Virutal items');
        if(!orderItemIdToOrderItemMap.isEmpty())
        {
            try{update orderItemIdToOrderItemMap.values();}
            catch(DMLException de)
            {
                Integer numErrors = de.getNumDML();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0; i<numErrors; i++)
                {
                    System.debug('getDMLFieldNames=' + de.getDMLFieldNames(i));
                    System.debug('getDMLFieldNames=' + de.getDMLMessage(i));
                }
            }
        }
        
        //Update the Orders
        system.debug('Entering process to update the Orders');
        if(!orderIdToOrderMap.isEmpty())
        {
            try{update orderIdToOrderMap.values();}
            catch(DMLException de)
            {
                Integer numErrors = de.getNumDML();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0; i<numErrors; i++)
                {
                    System.debug('getDMLFieldNames=' + de.getDMLFieldNames(i));
                    System.debug('getDMLFieldNames=' + de.getDMLMessage(i));
                }
            }
        }
    }
}