/****************************************************************************************************************
@Author        : Kumar Navneet
@Date Created    : 27/11/2019
@Description    : Test class for TrgCRMErrorEventIn 
****************************************************************************************************************/
@isTest()
private class ClsTestTrgCRMErrorEventIn  {
	
    @isTest private static void test(){
        
        List<Platform_Event_Error_Log__c> platfrm= new List<Platform_Event_Error_Log__c>();
        for(Integer i=0 ;i <200;i++)
        {
            Platform_Event_Error_Log__c pfe = new Platform_Event_Error_Log__c();
            pfe.Exception_Error__c ='Logging incoming Event'+i;
            platfrm.add(pfe);
        }
        
        insert platfrm;
        
        CRM_Error_Event_In__e  oEvent = new CRM_Error_Event_In__e (code__c = 'Test',entityType__c = 'Sales_Order', eventSource__c = 'update', payload__c ='Test',
                                                           		  message__c = 'ESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST');
        System.debug('====oEvent===='+JSON.serialize(oEvent));
        Test.startTest();        	
            Database.SaveResult sr = EventBus.publish(oEvent);
        Test.stopTest();
    }
   
}