public class SSIPScheduleJobUtiltiy {
    
    public class CalculateTaxQueueable implements Queueable, Database.AllowsCallouts{
        public String orderId;
        public String paymentType;
        public String countryCode;
        public CalculateTaxQueueable(String orderId, String paymentType, String countryCode){
            this.orderId = orderId;
            this.paymentType = paymentType;
            this.countryCode = countryCode;
        }
        public void execute(QueueableContext context) {
            try{
                String recordTypeName = countryCode + ' Sales Order Read Only';
                String result = ClsOUSTaxCalculator.calculateTax(orderId);
                if(!result.containsIgnoreCase('VAT/TAX Calculation Failed') && paymentType == 'net0' && !Test.isRunningTest()){
                    System.enqueueJob(new AuthorizeAndSettlePaymentQueueable(orderId, countryCode));
                }else if(!result.containsIgnoreCase('VAT/TAX Calculation Failed')){
                    if(Schema.SObjectType.Order.getRecordTypeInfosByName().containsKey(recordTypeName)){
                        update new Order(Id = orderId, Status = 'Shipping Hold', recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId());
                    }
                }
            }catch(Exception ex){
                ErrorUtility.LogError('Consume', 'SSIPScheduleJobUtiltiy', ex.getMessage(), 'SSIPScheduleJobUtiltiy', 'crm', '', orderId , 'Informational');
            }
        }
    }
    
    public class AuthorizeAndSettlePaymentQueueable implements Queueable, Database.AllowsCallouts{
        public String orderId;
        public String countryCode;
        
        public AuthorizeAndSettlePaymentQueueable(String orderId, String countryCode){
            this.orderId = orderId;
            this.countryCode = countryCode;
        }
        public void execute(QueueableContext context) {
            String recordTypeName = countryCode + ' Sales Order Read Only';
            Map<Id, ClsCCAuthAndSettlementResponse> mapResponse = ClsCCAuthAndSettlementService.authorizeAndSettlePayment(orderId);
            if(mapResponse.containsKey(orderId) && mapResponse.get(orderId).Status == ClsCCAuthAndSettlementResponse.enumStatus.SUCCESS){
                if(Schema.SObjectType.Order.getRecordTypeInfosByName().containsKey(recordTypeName)){
                    try{
                        ClsOrderHandlerStatic.executeOrderEventTriggerv11 = true;
                        update new Order(Id = orderId, Status = 'Activated', recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId());
                    }catch(Exception ex){
                        ErrorUtility.LogError('Consume', 'SSIPScheduleJobUtiltiy', ex.getMessage(), 'SSIPScheduleJobUtiltiy', 'crm', '', orderId , 'Informational');
                    }
                }
            }
        }
    }
}