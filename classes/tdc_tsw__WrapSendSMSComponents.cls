/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WrapSendSMSComponents {
    @AuraEnabled
    global Boolean enableFolder;
    @AuraEnabled
    global String lstFolderOption;
    @AuraEnabled
    global String lstSenderNumOption;
    @AuraEnabled
    global String lstSMSTemplate;
    @AuraEnabled
    global Map<String,String> mapDocs;
    @AuraEnabled
    global String messageText;
    @AuraEnabled
    global String recordId;
    @AuraEnabled
    global String selectedTemplate;
    @AuraEnabled
    global String senderPhoneKey;
    @AuraEnabled
    global String strPhoneApi;
    @AuraEnabled
    global String strToNumber;
    @AuraEnabled
    global List<String> unreadMsgIds;
    global WrapSendSMSComponents() {

    }
}
