public class DocuSignTemplateCon{
    @AuraEnabled
    public static DSTemplateWrapper getRecordDataType(Id recordId){
        DSTemplateWrapper dsCls = new DSTemplateWrapper();
        List<DocuSign_Templates__mdt> lstDT= new List<DocuSign_Templates__mdt>();        
        Schema.DescribeSObjectResult dr = recordId.getSobjectType().getDescribe();
        string objName=dr.getName();  dsCls.sObjectName = objName;
        
        String queryString = 'SELECT Id, RecordType.DeveloperName, Name FROM ' + dr.getName() + ' WHERE Id=\'' + recordId + '\''; 
        if(objName=='Opportunity'){
            queryString = 'SELECT Id, RecordType.DeveloperName, Name, Account.Name, Account.PersonEmail FROM ' + dr.getName() + ' WHERE Id=\'' + recordId + '\''; 
        } else if(objName=='Account'){
            queryString = 'SELECT Id, RecordType.DeveloperName, Name, PersonEmail FROM Account WHERE Id=\'' + recordId + '\''; 
        } 
              
        sObject[] objDBList = Database.query(queryString);
        string RTId=(string)objDBList[0].get('RecordTypeId');
        dsCls.sObjectRecordName =(string)objDBList[0].get('Name');        
        dsCls.RecepientName=dsCls.sObjectRecordName;
         
        
        if(objName=='Opportunity'){
           dsCls.RecepientName=(string)objDBList[0].getSobject('Account').get('Name');
           dsCls.RecepientEmail=(string)objDBList[0].getSobject('Account').get('PersonEmail');
        } else if(objName=='Account'){
            dsCls.RecepientEmail=(string)objDBList[0].get('PersonEmail');
        }      
        
        string recordType=null; recordType=(string)objDBList[0].getSobject('RecordType').get('DeveloperName');
        if(objName != null && recordType!= null){
            dsCls.lstDSConfig = new List<DocuSign_Templates__mdt>([Select Template_Id__c, Template_Name__c, Email_Subject__c, Email_Message__c From DocuSign_Templates__mdt Where Object__c=:objName 
            AND Record_Type__c=:recordType AND Active__c=True]);
        }
        
        return dsCls;        
    }
    
  public class DSTemplateWrapper {
    @AuraEnabled
    public list<DocuSign_Templates__mdt> lstDSConfig {get;set;}    
    @AuraEnabled
    public string sObjectName {get; set;}
    @AuraEnabled    
    public string sObjectRTName {get; set;}
    @AuraEnabled    
    public string sObjectRecordName {get; set;}
    @AuraEnabled    
    public string RecepientName {get; set;}
    @AuraEnabled    
    public string RecepientEmail {get; set;}
    
    public DSTemplateWrapper(){
        this.lstDSConfig =new  list<DocuSign_Templates__mdt>();   
    }
  }
  
  @AuraEnabled
  public static List<dfsle.Envelope.Status> SendDocument(Id entityId, string templateId, string recipientName, string recepientEmail, string emailSubject, string emailMessage, string recepientId){
    // Create an empty envelope.
   dfsle.Envelope myEnvelope = dfsle.EnvelopeService.getEmptyEnvelope(new dfsle.Entity(entityId));
    
     
    //use the Recipient.fromSource method to create the Recipient
    dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
            recipientName, // Recipient name
            recepientEmail, // Recipient email
            null, //Optional phone number
            'Signer 1', //Role Name. Specify the exact role name from template
            new dfsle.Entity(recepientId)); //source object for the Recipient
    
    
    dfsle.Recipient.EmailSettings es= new dfsle.Recipient.EmailSettings('en','en',emailSubject,emailMessage);
    
   dfsle.Recipient myRecipient1 = new dfsle.Recipient( null, null, null, null, null, recipientName, recepientEmail, null, null, null, null, new dfsle.Recipient.EmailSettings('en','en',emailSubject,emailMessage), null, null, null, new dfsle.Entity(recepientId), true, true);
    
    
    //add Recipient to the Envelope
    myEnvelope = myEnvelope.withRecipients(new List<dfsle.Recipient> { myRecipient1 });
    
    //myTemplateId contains the DocuSign Id of the DocuSign Template
    dfsle.UUID myTemplateId = dfsle.UUID.parse(templateId);

    //create a new document for the Envelope
    dfsle.Document myDocument = dfsle.Document.fromTemplate(
        myTemplateId, // templateId in dfsle.UUID format
        'myTemplate'); // name of the template

    //add document to the Envelope
    myEnvelope = myEnvelope.withDocuments(new List<dfsle.Document> { myDocument });
    
    // Send the envelope.
    myEnvelope = dfsle.EnvelopeService.sendEnvelope(myEnvelope, // The envelope to send
     true); // Send now?
   System.Debug('*** TOS:EH 1.1 Sent');
   
   List<dfsle.Envelope.Status> myStatus = dfsle.StatusService.getStatus(
    new Set<Id> { // IDs of the Salesforce objects with associated status.
        entityId
    },
    1); // Maximum number of records to return.
     System.Debug('*** TOS:EH 2.1 status=' + myStatus);
     
     return myStatus;
  }
  
  @AuraEnabled
  public static void SendDocument1(Id entityId, string templateId, string recipientName, string recepientEmail, string emailSubject, string emailMessage, string recepientId){
    
    dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(recipientName,recepientEmail, null, 'Signer 1', new dfsle.Entity(recepientId));
    dfsle.UUID myTemplateId = dfsle.UUID.parse(templateId);
    dfsle.Document myDocument =dfsle.Document.fromTemplate(myTemplateId, 'myTemplate');
    dfsle.Envelope myEnvelope = new dfsle.Envelope(
                               null,
                               null,
                               null,
                               null,
                               new List<dfsle.Document> { myDocument },
                               null,
                               null,
                               null,
                               emailSubject,
                               emailMessage,
                               null,
                               null);
    myEnvelope = myEnvelope.withRecipients(new List<dfsle.Recipient> { myRecipient });   
    myEnvelope = dfsle.EnvelopeService.sendEnvelope(myEnvelope, true); 
    System.Debug('*** TOS:EH 1.1 Sent');
  }
  

  @AuraEnabled
  public static List<dfsle.Envelope.Status> GetDocumentStatus(Id entityId){
    List<dfsle.Envelope.Status> myStatus = dfsle.StatusService.getStatus(
    new Set<Id> { entityId }, 1); // Maximum number of records to return.
    System.Debug('*** TOS:EH 2.1 status=' + myStatus);     
    return myStatus; 
  }
    
    

}