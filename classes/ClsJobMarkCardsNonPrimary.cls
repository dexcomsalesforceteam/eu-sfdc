public class ClsJobMarkCardsNonPrimary implements Queueable {
    private List<Finance_Detail__c> lstFD;
    public ClsJobMarkCardsNonPrimary(List<Finance_Detail__c> lst) {
        this.lstFD= lst;       
    }
    public void execute(QueueableContext context) {
        List<Finance_Detail__c> lstFDUpd= new List<Finance_Detail__c>();
        for(Finance_Detail__c redFD : lstFD){
            for(Finance_Detail__c fd : [Select Id, Primary__C from Finance_Detail__c where Id !=:redFD.Id AND Account__c=:redFD.Account__c AND Primary__c=true]){
                fd.Primary__c=false;
                lstFDUpd.Add(fd);                
            }
        } 
        
        if(lstFDUpd.size()>0){
            update lstFDUpd;    
        }
    }
}