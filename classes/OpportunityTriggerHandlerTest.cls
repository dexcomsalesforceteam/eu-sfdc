/*********************************************************
* @author      : Kingsley Tumaneng
* @date        : SEPT 9 2015
* @description : Test class for OpportunityTriggerHandler
*********************************************************/
@isTest
private class OpportunityTriggerHandlerTest {
    private static Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
    
    //ADDED BY Noy De Goma@CSHERPAS on 01.07.2016
    //-- START
    @testSetup static void testData(){
        User systAd = TestDataBuilder.testUser();
        system.runAs(systAd){
            Territory_Opportunity_Team_Role__c totc = new Territory_Opportunity_Team_Role__c(
                SA__c               = 'SA - Sales Administrator',
                PCS__c              = 'PCS - Patient Care Specialist',          
                Admin__c            = 'Administrative Assistant',                 
                MCS__c              = 'MCS - Manaaged Care Specialist',          
                DBM__c              = 'DBM - District Sales Manager',         
                RSD__c              = 'RSD - Regional Sales Director',        
                Territory_Rep__c    = 'Temporary TBM'
            );
            insert totc;
            
            Account acc = TestDataBuilder.getAccountList(1, recId)[0];
            acc.Territory_Code__c = '1234';
            insert acc;
            
            list <User> userList = new list <User>();
            for(Integer i = 0; i < 7; i++){
                User u = TestDataBuilder.getUser('System Administrator', 'User' + i + 'Test');
                userList.add(u);
            }
            insert userList;
            
            Territory_Alignment__c territory = TestDataBuilder.getTerritory('1234', userList);
            insert territory;
        }
    }
    
    private static testMethod void createOpportunityTeamTestScenario1(){
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        Account acc = [SELECT Id FROM Account WHERE Name  = 'Test Account Name0' limit 1];
        Opportunity opp = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id)[0];
        test.startTest();
        insert opp;
        test.stopTest();
        
        list <OpportunityTeamMember> oppTeamList = [SELECT Id, UserId, TeamMemberRole FROM OpportunityTeamMember];
        System.assertEquals(7, oppTeamList.size());
        
        
        for (OpportunityTeamMember mem : oppTeamList){
            if(mem.TeamMemberRole == totcCS.SA__c){
                System.assertEquals(ta.SA__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.PCS__c){
                System.assertEquals(ta.PCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Admin__c){
                System.assertEquals(ta.Admin__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.MCS__c){
                System.assertEquals(ta.MCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.DBM__c){
                System.assertEquals(ta.DBM__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.RSD__c){
                System.assertEquals(ta.RSD__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                System.assertEquals(ta.Territory_Rep__c, mem.UserId);
            }
        }
    }
    
    private static testMethod void createOpportunityTeamTestScenario2(){
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        list <Opportunity> oppList = new list <Opportunity>();
        Account acc = [SELECT Id FROM Account WHERE Name  = 'Test Account Name0' limit 1];
        oppList = TestDataBuilder.getOpportunityList(2, acc.Id, acc.Id);
        test.startTest();
        insert oppList;
        test.stopTest();
        
        
        for (Opportunity opp : [SELECT Id, (SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMembers) FROM Opportunity]){
            System.assertEquals(7, opp.OpportunityTeamMembers.size());
            for (OpportunityTeamMember mem : opp.OpportunityTeamMembers){
                if(mem.TeamMemberRole == totcCS.SA__c){
                    System.assertEquals(ta.SA__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.PCS__c){
                    System.assertEquals(ta.PCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Admin__c){
                    System.assertEquals(ta.Admin__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.MCS__c){
                    System.assertEquals(ta.MCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.DBM__c){
                    System.assertEquals(ta.DBM__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.RSD__c){
                    System.assertEquals(ta.RSD__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                    System.assertEquals(ta.Territory_Rep__c, mem.UserId);
                }
            }
        }
    }
    
    private static testMethod void createOpportunityTeamTestScenario3(){
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        list <Opportunity> oppList = new list <Opportunity>();
        Account acc = [SELECT Id FROM Account WHERE Name  = 'Test Account Name0' limit 1];
        User u =  [SELECT id FROM User WHERE username = 'TestUser@hesser.com' limit 1];
        User u2 =  [SELECT id, IsActive FROM User WHERE username = 'User0Test@hesser.com'];
        system.runAs(u){
            
            u2.IsActive = false;
            
            update u2;
        }
        
        oppList = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id);
        test.startTest();
        insert oppList;
        test.stopTest();
        list <OpportunityTeamMember> oppTeamList = [SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMember];
        System.assertEquals(6, oppTeamList.size());
        for (OpportunityTeamMember mem : oppTeamList){
            if(mem.TeamMemberRole == totcCS.SA__c){
                System.assertEquals(ta.SA__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.PCS__c){
                System.assertEquals(ta.PCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Admin__c){
                System.assertEquals(ta.Admin__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.MCS__c){
                System.assertEquals(ta.MCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.DBM__c){
                System.assertEquals(ta.DBM__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.RSD__c){
                System.assertEquals(ta.RSD__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                System.assertEquals(ta.Territory_Rep__c, mem.UserId);
            }
        }
    }  
    
    
    
    private static testMethod void createOpportunityTeamTestScenario4(){
        
        List<Opportunity> oppList = New List<Opportunity>();
        
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        ta.SA__c = null;
        ta.MCS__c = null;
        
        update ta;
        
        list <Opportunity> oppListForParameter = new list <Opportunity>();
        Account acc = [SELECT Id FROM Account WHERE Name  = 'Test Account Name0' limit 1];
        User u =  [SELECT id FROM User WHERE username = 'TestUser@hesser.com' limit 1];
        
        oppList = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id);
        test.startTest();
        insert oppList;
        
        list <OpportunityTeamMember> oppTeamList = [SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMember];
        System.assertEquals(5, oppTeamList.size());
        for (OpportunityTeamMember mem : oppTeamList){
            if(mem.TeamMemberRole == totcCS.SA__c){
                System.assertEquals(ta.SA__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.PCS__c){
                System.assertEquals(ta.PCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Admin__c){
                System.assertEquals(ta.Admin__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.MCS__c){
                System.assertEquals(ta.MCS__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.DBM__c){
                System.assertEquals(ta.DBM__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.RSD__c){
                System.assertEquals(ta.RSD__c, mem.UserId);
            }
            if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                System.assertEquals(ta.Territory_Rep__c, mem.UserId);
            }
        }
        
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId();
        Map<id, Opportunity> oppMap = New Map<id, Opportunity>();
        Account accForOpp = New Account();
        accForOpp.FirstName = 'FirstName';
        accForOpp.lastName = 'LastName';
        insert accForOpp;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpportunityName';
        opp.AccountId = accForOpp.ID;
        opp.Type = 'NEW SYSTEM';
        opp.CloseDate = System.Today();
        opp.StageName = '1. New Opportunity';
        opp.RecordTypeId = devRecordTypeId;
        insert opp;
		//Jagan 11/15/2018 Added below block to test ProcessAccountFieldsUpdateEventTrigger code
		//--Start--
		opp.MDCR_Communication_Preference__c = 'Phone';
		update opp;
        test.getEventBus().deliver(); 
		accForOpp.MDCR_Communication_Preference__c = 'Email';
		update accForOpp;
		//--End--
		opp.CloseDate = System.Today();
        opp.StageName = '61. Quote Approved';
		update opp;
        oppListForParameter.add(opp); 
        
        for(Opportunity opty: oppListForParameter){
            oppMap.put(opty.id,opty);           
        }
      //  Update oppListForParameter;
        //OpportunityTriggerHandler oppTrigerHandler = OpportunityTriggerHandler();
      OpportunityTriggerHandler.beforeUpdate(oppListForParameter);
      OpportunityTriggerHandler.afterUpdate(oppListForParameter,oppMap);
      OpportunityTriggerHandler.calculateCopayOnUpdate(oppListForParameter,oppMap);
     /*  UtilityClass.runBeforeTrigger = false; 
       opp.Last_MDCR_Reorder_Follow_up_Date__c = System.today();
       Update opp; */
       test.stopTest();
    }
}