/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ConversationView_LightningController {
    global ConversationView_LightningController() {

    }
    @AuraEnabled
    global static String createRecord(String selectedIds, String objVarJson) {
        return null;
    }
    @AuraEnabled
    global static String endChat(String recordId, String senderkey) {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError getMessagelst(String objDataVar, String strdate) {
        return null;
    }
    global static String getObjectDefaultPhnApi(String currentObject) {
        return null;
    }
    @AuraEnabled
    global static Integer getRefreshTimeInterval() {
        return null;
    }
    @AuraEnabled
    global static String getSessionId() {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError getUserlst(String recordId) {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError getWrapSendSMSComponentsData(String recordId) {
        return null;
    }
    @AuraEnabled
    global static Boolean gotNewIncoming(String smsId, String recordId) {
        return null;
    }
    @AuraEnabled
    global static Boolean isTextAreaEditable(String flag) {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError markasImportant(String recordId, Boolean isImportant) {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError onChangeSMSTemp(String selectedTemplate, String recordId) {
        return null;
    }
    @AuraEnabled
    global static tdc_tsw.WrapResultWithError sendSMS(String objWprCompJson) {
        return null;
    }
}
