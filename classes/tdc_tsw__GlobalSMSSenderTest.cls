/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@isTest
global class GlobalSMSSenderTest {
    global GlobalSMSSenderTest() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void smsSender() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void smsSender2() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testUnit10() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testUnit11() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testUnit7() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testUnit8() {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testUnit9() {

    }
}
