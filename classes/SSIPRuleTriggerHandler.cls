/**********************************************************************
@Description    : Class will handle logic for SSIPRule object
***********************************************************************/
public class SSIPRuleTriggerHandler {
    
    
    public void onAfterInsert(List<SSIP_Rule__c> lstSSIPRuleNew){
        createSSIPScheduleRecord(lstSSIPRuleNew, new Map<Id,SSIP_Rule__c>());
    }
    
    public void onAfterUpdate(List<SSIP_Rule__c> lstSSIPRuleNew, Map<Id, SSIP_Rule__c> mapSSIPRuleOld){
        createSSIPScheduleRecord(lstSSIPRuleNew, mapSSIPRuleOld);
    }
    
    
    private void createSSIPScheduleRecord(List<SSIP_Rule__c> lstSSIPRuleNew, Map<Id, SSIP_Rule__c> mapSSIPRuleOld){
        Map<Id, SSIP_Rule__c> mapSSIPRuleNew = new Map<Id, SSIP_Rule__c>(lstSSIPRuleNew);
        Map<Id, Id> ruleToFundMap = new Map<Id, Id>();		
        Map<Id, List<SSIP_Schedule__c>> mapSSIPIdTolstSSIPSchedule = new Map<Id, List<SSIP_Schedule__c>>();
        List<SSIP_Schedule__c> lstSSIPSchedule = new List<SSIP_Schedule__c>();
        List<SSIP_Schedule__c> lstSSIPScheduleToDelete = new List<SSIP_Schedule__c>();
        Set<Id> setSSIPRuleId = new Set<Id>();
        
        //Find if the Rule has order association and prepare the map
        for(SSIP_Rule__c objSSIPRule : [SELECT Id, First_Order__r.Fund__c FROM SSIP_Rule__c WHERE First_Order__c != null AND Id IN: lstSSIPRuleNew]){
            if(objSSIPRule.First_Order__r.Fund__c != null)
                ruleToFundMap.put(objSSIPRule.Id, objSSIPRule.First_Order__r.Fund__c);
        }
        //Process the SSIP Rule records to create Schedules
        for(SSIP_Rule__c objSSIPRule : lstSSIPRuleNew){
            Integer count = objSSIPRule.Schedule_Count__c != null ? Integer.valueOf(objSSIPRule.Schedule_Count__c) : 3;
            Integer firstRepeatOrderLeadDays = objSSIPRule.First_Repeat_Order_Lead_Days__c != null ? Integer.valueOf(objSSIPRule.First_Repeat_Order_Lead_Days__c) : 7;
            if((Trigger.isInsert && objSSIPRule.First_Shipment_Date__c != null) || (Trigger.isUpdate && mapSSIPRuleOld.get(objSSIPRule.Id).First_Shipment_Date__c == null && objSSIPRule.First_Shipment_Date__c != null) && objSSIPRule.Frequency_In_Days__c != null){
                for(Integer i = 1; i <= count; i++){
                    lstSSIPSchedule.add(new SSIP_Schedule__c(Account__c = objSSIPRule.Account__c, Country__c = objSSIPRule.Country__c, Payment_Term__c = objSSIPRule.Payment_Term__c, Shipping_Address__c = objSSIPRule.Shipping_Address__c,
                                                             Shipping_Method__c = objSSIPRule.Shipping_Method__c, SSIP_Rule__c = objSSIPRule.Id, Status__c = 'Open',
                                                             Fund__c = !ruleToFundMap.isEmpty() ? ruleToFundMap.get(objSSIPRule.Id) : null, 	
                                                             Schedule_Date__c = objSSIPRule.First_Shipment_Date__c.addDays(Integer.valueOf(objSSIPRule.Frequency_In_Days__c) * i) - firstRepeatOrderLeadDays));
                }
            }
            if(Trigger.isUpdate && ((mapSSIPRuleOld.get(objSSIPRule.Id).First_Shipment_Date__c != null && objSSIPRule.Rule_End_Date__c != mapSSIPRuleOld.get(objSSIPRule.Id).Rule_End_Date__c) 
                                    || mapSSIPRuleOld.get(objSSIPRule.Id).Payment_Term__c != objSSIPRule.Payment_Term__c || mapSSIPRuleOld.get(objSSIPRule.Id).Shipping_Method__c != objSSIPRule.Shipping_Method__c)){
                                        setSSIPRuleId.add(objSSIPRule.Id);
                                    }
        }
        //Set Payment Term, Shipping Method from the Rule to the schedule records. Also delete all schedules once the Rule End Date is set on the Rule
        for(SSIP_Schedule__c objSSIPSchedule : [SELECT Id, SSIP_Rule__c, SSIP_Rule__r.Rule_End_Date__c, Rescheduled_Shipment_Date__c, Schedule_Date__c FROM SSIP_Schedule__c WHERE SSIP_Rule__c IN: setSSIPRuleId AND Status__c = 'Open']){
            //Process deletion logic first and then go to update logic
            //If the Reschedued Shipment Date is populated on the SSIP Schedule that takes the precedence
            if(objSSIPSchedule.Rescheduled_Shipment_Date__c != null && (objSSIPSchedule.Rescheduled_Shipment_Date__c > objSSIPSchedule.SSIP_Rule__r.Rule_End_Date__c))
                lstSSIPScheduleToDelete.add(objSSIPSchedule);
            else
                if(objSSIPSchedule.Rescheduled_Shipment_Date__c == null && (objSSIPSchedule.Schedule_Date__c > objSSIPSchedule.SSIP_Rule__r.Rule_End_Date__c))
                lstSSIPScheduleToDelete.add(objSSIPSchedule);
            else{
                if(mapSSIPRuleNew.get(objSSIPSchedule.SSIP_Rule__c).Payment_Term__c != mapSSIPRuleOld.get(objSSIPSchedule.SSIP_Rule__c).Payment_Term__c){
                    objSSIPSchedule.Payment_Term__c = mapSSIPRuleNew.get(objSSIPSchedule.SSIP_Rule__c).Payment_Term__c;
                }
                if(mapSSIPRuleNew.get(objSSIPSchedule.SSIP_Rule__c).Shipping_Method__c != mapSSIPRuleOld.get(objSSIPSchedule.SSIP_Rule__c).Shipping_Method__c){
                    objSSIPSchedule.Shipping_Method__c = mapSSIPRuleNew.get(objSSIPSchedule.SSIP_Rule__c).Shipping_Method__c;
                }
                lstSSIPSchedule.add(objSSIPSchedule);
            }
        }
        
        delete lstSSIPScheduleToDelete;
        upsert lstSSIPSchedule;
    }
}