/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WrapDataVariables {
    @AuraEnabled
    global Date filterdate;
    @AuraEnabled
    global String filtereCriteria;
    @AuraEnabled
    global Boolean isFiltered;
    @AuraEnabled
    global String ownerId;
    @AuraEnabled
    global String recordId;
    @AuraEnabled
    global Boolean starred;
    @AuraEnabled
    global String strPhoneApi;
    @AuraEnabled
    global Boolean unread;
    global WrapDataVariables() {

    }
}
