public class PEClsHandleOrderItemEvents {
	public static final String version = '1.0';  // version of the canonical
    public static void handleIncomingEvents(List<CRM_OrderItem_Event__e> incEvs) {
        List<CRM_OrderItem_Event__e> consumeUpdateOIList = new List<CRM_OrderItem_Event__e>();
        for (CRM_OrderItem_Event__e ev : incEvs) {
            //For debugging purposes - next line
            system.debug('Incoming OrderItem Event: ' + ev);
            if ( ev.operation__c.containsIgnoreCase('UPDATE') && ev.stage__c.containsIgnoreCase('PENDING') && ev.eventSource__c.containsIgnoreCase('OMS') )
                consumeUpdateOIList.add(ev); 
            else if (ev.eventSource__c.containsIgnoreCase('CRM')){
                System.debug('Ignoring an event from Self');
            } else {
                ErrorUtility.LogError('Consume', 'PEClsHandleOrderItemEvents.handleIncomingEvents', 'Error in Consuming OrderItem Event', 'orderItem', incEvs[0].eventSourceID__c, 
                                          'Unable to process an incoming orderItem event - Don\'t know how to handle it. eventSource: ' + ev.eventSource__c + ', eventSourceID: ' + ev.eventSourceID__c + ' Will not be processed.' , 
                                          incEvs[0].eventSourceID__c, 'Informational');
            }
        }
        if (consumeUpdateOIList.size() > 0) consumeOrderItemUpdate(consumeUpdateOIList, 'update');
    }
    public static void consumeOrderItemUpdate(List<CRM_OrderItem_Event__e> oiEvents, String op) {
        // To do
        List<OrderItem> ois = new List<OrderItem>();
        OrderItem oi = new OrderItem();
        Set<Id> oiSet = new Set<Id>();
        for(CRM_OrderItem_Event__e ev : oiEvents) {
            // Need OrderItem ID to update an OrderItem. 
            if (String.isBlank(ev.itemID__c) || ev.itemID__c == 'null')  {
				// Publish an error message as this OrderItem can't be updated!
                ErrorUtility.LogError('Consume', 'PEClsHandleOrderItemEvents.consumeOrderItemUpdate', 'Error in Consuming OrderItem Update Events.', 'orderItem', ev.eventSourceID__c, 
                                      'Unable to consume an OrderItem Update. Order Item Id is not found in the incoming Event. eventSource: ' + ev.eventSource__c + ', eventSourceID: ' + ev.eventSourceID__c + ' Will not be processed.' , 
                                      ev.eventSourceID__c, 'Informational');
            } else {oiSet.add(ev.itemID__c);}
        } 
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem>();
        if (oiSet.size() > 0 ) {
            List<OrderItem> orderItemsList = [Select ID, order.OrderNumber from OrderItem where Id in :oiSet];
            for (OrderItem a: orderItemsList) orderItemMap.put(a.Id, a);
        }
        
        for(CRM_OrderItem_Event__e ev : oiEvents) {
            oi = new OrderItem();
            if (String.isBlank(ev.itemID__c) || ev.itemID__c == 'null') {
                continue;
            }
            if (orderItemMap.containsKey(ev.itemID__c))   oi.id = ev.itemID__c;
            else {
				ErrorUtility.LogError('Consume', 'PEClsHandleOrderItemEvents.consumeOrderItemUpdate', 'Error in Consuming OrderItem Update Events.', 'orderItem', ev.eventSourceID__c, 
                                      'Unable to consume an OrderItem Update. Unknown Item Id is found in the incoming Event. eventSource: ' + ev.eventSource__c + ', eventSourceID: ' + ev.eventSourceID__c + ' Will not be processed.' , 
                                      ev.eventSourceID__c, 'Informational');
                continue;
            }
            if (ev.status__c != 'null' && !String.isBlank(ev.status__c))  oi.Status__c = ev.status__c;  // probably will need mapping .....*************************
            if (ev.orderNumber__c != 'null' && !String.isBlank(ev.orderNumber__c) && orderItemMap.get(ev.itemID__c).order.OrderNumber != ev.orderNumber__c)   {
                				ErrorUtility.LogError('Consume', 'PEClsHandleOrderItemEvents.consumeOrderItemUpdate', 'Error in Consuming OrderItem Update Events.', 'orderItem', ev.eventSourceID__c, 
                                      'Unable to consume an OrderItem Update. Order Number does not correspond to the Order Item in the incoming Event. eventSource: ' + ev.eventSource__c + ', eventSourceID: ' + ev.eventSourceID__c + ' Will not be processed.' , 
                                      ev.eventSourceID__c, 'Informational');
                continue;
            }
            if (ev.shippingProvider__c != 'null' && !String.isBlank(ev.shippingProvider__c))   oi.Shipping_Provider__c = ev.shippingProvider__c;  // May need mapping.
            if (ev.trackingNumber__c != 'null' && !String.isBlank(ev.trackingNumber__c))   oi.Tracking_Number__c = ev.trackingNumber__c;  
            if (ev.deliveryNumber__c != 'null' && !String.isBlank(ev.deliveryNumber__c))   oi.Delivery_Number__c = ev.deliveryNumber__c;  
            if (ev.shippedDate__c != 'null' && !String.isBlank(ev.shippedDate__c))   oi.Actual_Ship_Date__c = Date.valueOf(ev.shippedDate__c); 
            if (ev.shippedQuantity__c != null)   oi.Shipped_Qty__c = ev.shippedQuantity__c;
            if (ev.serialNumber__c != 'null' && !String.isBlank(ev.serialNumber__c))   oi.Serial_Number__c = ev.serialNumber__c;  
            if (ev.lotNumber__c != 'null' && !String.isBlank(ev.lotNumber__c))   oi.Lot_Number__c = ev.lotNumber__c;  
            if (ev.lotExpirationDate__c != 'null' && !String.isBlank(ev.lotExpirationDate__c))   oi.Lot_Expiration_Date__c = ev.lotExpirationDate__c;  
			ois.add(oi);
        }
        if (ois.size() > 0) {
            try {
                Database.update(ois, true);
            }
            catch(DmlException de) {
                system.debug(de.getMessage());
                ErrorUtility.LogError('Consume', 'PEClsHandleAddressEvents.consumeAddressUpdate', 'Error in Consuming Address Update Operations.', 'address', oiEvents[0].eventSourceID__c, 
                     de.getMessage(), oiEvents[0].eventSourceID__c, 'Fatal Error - Bailing out');
                return;
            }
        }
        oiSet.clear();
        for(OrderItem o : ois) oiSet.add(o.id);
		publishOrderItemUpdate(oiSet, 'update');
    }
    public static void publishOrderItemUpdate(Set<Id> oiSet, String op) {
        List<CRM_OrderItem_Event_Out__e> adds = new List<CRM_OrderItem_Event_Out__e>();
        for(OrderItem a : [ select  status__c, ID, order.OrderNumber, Tracking_Number__c, order.System_Of_Origin__c, order.System_Of_Origin_Id__c, Delivery_Number__c, Actual_Ship_Date__c , Shipped_Qty__c, Serial_Number__c,
                       			Lot_Number__c, Lot_Expiration_Date__c, LastModifiedDate, LastModifiedBy.Name, Shipping_Provider__c     
            from OrderItem where Id in :oiSet]) {
            	CRM_OrderItem_Event_Out__e ae = new CRM_OrderItem_Event_Out__e();
                // Header
                ae.version__c = version;
                //ae.eventDate__c =  String.valueOf(a.LastModifiedDate); //String.valueOf(System.now());
                ae.eventDate__c =  String.valueOf(a.LastModifiedDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'));
                ae.entityType__c = 'orderItem';
                ae.eventSource__c = 'crm';
                ae.eventSourceID__c = a.Id;
                ae.operation__c = op;
                ae.stage__c = 'confirmed';
                ae.systemOfOrigin__c = a.order.System_Of_Origin__c;
                ae.systemOfOriginID__c = a.order.System_Of_Origin_Id__c;
                ae.parentSystemOfOriginID__c = '';   
                ae.author__c = a.LastModifiedBy.Name;
                ae.status__c =  a.Status__c;     // probably needs mapping
                ae.orderNumber__c = a.order.OrderNumber;
                ae.itemID__c = a.Id;
                ae.shippingProvider__c = a.Shipping_Provider__c;
                ae.trackingNumber__c = a.Tracking_Number__c;
                ae.deliveryNumber__c = a.Delivery_Number__c;
                ae.shippedDate__c = String.valueOf(a.Actual_Ship_Date__c);
                ae.shippedQuantity__c = a.Shipped_Qty__c;
                ae.serialNumber__c = a.Serial_Number__c;
                ae.lotNumber__c = a.Lot_Number__c;
                ae.lotExpirationDate__c = a.Lot_Expiration_Date__c;
                adds.add(ae);
        }
        if (adds.size() > 0) {
            List<Database.SaveResult> svrs = EventBus.publish(adds);
        	for (Database.SaveResult svr : svrs) {
                if (!svr.isSuccess()) {
                    ErrorUtility.LogError('Publish', 'PEClsHandleOrderItemEvents.publishOrderItemUpdate', 'Error in Publishing OrderItem Event', 'orderItem', '', 
                                          'Unable to publish an orderItem Event. Error: ' + svr.getErrors(), 
                                          '', 'Informational');
                }
            }
          }
    }
}