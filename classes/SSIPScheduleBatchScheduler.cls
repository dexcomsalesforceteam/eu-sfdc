global class SSIPScheduleBatchScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new SSIPScheduleBatch(), 10);
    }
}