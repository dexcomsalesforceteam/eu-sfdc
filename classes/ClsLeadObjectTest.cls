@isTest
public class ClsLeadObjectTest {
	
    @isTest
    private static void testLeadObject() {
      	Id leadRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE 'CA_%' AND SobjectType = 'Lead'].Id ;
        insert new Lead(Status = 'New', RecordTypeId = leadRecordTypeId, FirstName = 'Canada', LastName='Lead');
        System.assertNotEquals(Null, Lead.Id);
    }
    
   @isTest
    private static void testLeadObjectIE() {
      	Id leadRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName LIKE 'IE_%' AND SobjectType = 'Lead'].Id ;
        insert new Zip_to_Terr__c (name = 'P36', Territory__c = 'IE020101', District__c = 'IE020100', Country__c='Ireland', Region__c='IE020000');
        Lead Le =  new Lead(Status = 'New', RecordTypeId = leadRecordTypeId, FirstName = 'Ireland', LastName='Lead', PostalCode='P36', Street='test',  City='test', Country='Ireland');
        insert Le;
      //  Le.PostalCode = 'P37';
      //  update Le ;        
        System.assertNotEquals(Null, Lead.Id);
    }
    
    
}