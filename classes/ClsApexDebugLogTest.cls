@isTest
public class ClsApexDebugLogTest{
    testMethod
    static void createErrorLog(){
        try{
            Integer result = 1 / 0;
        }
        catch(Exception ex){
            new ClsApexDebugLog().createLog(
                new ClsApexDebugLog.Error(
                    'ClsApexDebugLog_Test',
                    'createErrorLog',
                    NULL,
                    ex
                )
            );

            List<Apex_Debug_Log__c> lstLogsCreated = [
                SELECT  Id, Type__c, Apex_Class__c, Method__c
                FROM    Apex_Debug_Log__c
                WHERE   Method__c = 'createErrorLog'
            ];

            System.assertEquals(1, lstLogsCreated.size());
            System.assertEquals('Error', lstLogsCreated.get(0).Type__c);
            System.assertEquals('ClsApexDebugLog_Test', lstLogsCreated.get(0).Apex_Class__c);
        }
    }

    testMethod
    static void createInformationLog(){
        new ClsApexDebugLog().createLog(
            new ClsApexDebugLog.Information(
                'ClsApexDebugLog_Test',
                'createInformationLog',
                NULL,
                'Logging Information from an Apex Class - ClsApexDebugLog_Test'
            )
        );

        List<Apex_Debug_Log__c> lstLogsCreated = [
            SELECT  Id, Type__c, Apex_Class__c, Method__c, Message__c
            FROM    Apex_Debug_Log__c
            WHERE   Method__c = 'createInformationLog'
        ];

        System.assertEquals(1, lstLogsCreated.size());
        System.assertEquals('Information', lstLogsCreated.get(0).Type__c);
        System.assertEquals('ClsApexDebugLog_Test', lstLogsCreated.get(0).Apex_Class__c);
        System.assertEquals('Logging Information from an Apex Class - ClsApexDebugLog_Test', lstLogsCreated.get(0).Message__c);
    }

    testMethod
    static void ws_createErrorLog(){
        try{
            Integer result = 1 / 0;
        }
        catch(Exception ex){
            ClsApexDebugLog.createLog(
                '{"Type" : "Error","ApexClass" : "ClsApexDebugLog_Test","Method" : "createErrorLog","RecordId" : "","Message" : "System.MathException: Divide by 0","StackTrace" : "Line: 1, Column: 1 System.MathException: Divide by 0"}'
            );

            List<Apex_Debug_Log__c> lstLogsCreated = [
                SELECT  Id, Type__c, Apex_Class__c, Method__c
                FROM    Apex_Debug_Log__c
                WHERE   Method__c = 'createErrorLog'
            ];

            System.assertEquals(1, lstLogsCreated.size());
            System.assertEquals('Error', lstLogsCreated.get(0).Type__c);
            System.assertEquals('ClsApexDebugLog_Test', lstLogsCreated.get(0).Apex_Class__c);
        }
    }

    testMethod
    static void ws_createInformationLog(){
        ClsApexDebugLog.createLog(
            '{"Type" : "Information","ApexClass" : "ClsApexDebugLog_Test","Method" : "createInformationLog","RecordId" : "","Message" : "Logging Information from an Apex Class - ClsApexDebugLog_Test"}'
        );

        List<Apex_Debug_Log__c> lstLogsCreated = [
            SELECT  Id, Type__c, Apex_Class__c, Method__c, Message__c
            FROM    Apex_Debug_Log__c
            WHERE   Method__c = 'createInformationLog'
        ];

        System.assertEquals(1, lstLogsCreated.size());
        System.assertEquals('Information', lstLogsCreated.get(0).Type__c);
        System.assertEquals('ClsApexDebugLog_Test', lstLogsCreated.get(0).Apex_Class__c);
        System.assertEquals('Logging Information from an Apex Class - ClsApexDebugLog_Test', lstLogsCreated.get(0).Message__c);
    }
}