/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrgSMSConfig {
    global String country;
    global String countryAreaCode;
    global Boolean disableIncomingSetup;
    global Boolean enableLinkTracking;
    global Boolean isAutomationOff;
    global String key;
    global Boolean linkTracking;
    global String msgServiceId;
    global String provider;
    global Integer remainingCredit;
    global Boolean showUsageInApp;
    global Integer smsCount;
    global String SMSNumber;
    global String status;
    global String token;
    global Integer totalCredit;
    global Integer totalLicenseOfNumber;
    global Date trialExpireDate;
    global Integer usedCredit;
    global OrgSMSConfig(SObject objConfig) {

    }
    global OrgSMSConfig(tdc_tsw__OrgSMSConfig__c objConfig) {

    }
    global static tdc_tsw.OrgSMSConfig findInstanceOfOrgSMSConfig(String senderPhoneKey) {
        return null;
    }
    global static List<tdc_tsw.OrgSMSConfig> findOrgSMSConfigRecordsList() {
        return null;
    }
    global static List<String> spitContentByDelimiter(String fileData, String rowDelimiter, String optionalKeyword) {
        return null;
    }
    global static void updateOrgSMSConfigInstance(String senderPhoneKey, Integer smsCount) {

    }
    global static SObject updateSmsAppNumber(SObject config, String secretkey) {
        return null;
    }
}
