public class OpportunityTriggerHandler {
    /****************************************************************************
* Author       : Scottie OBryan
* Date         : JAN 12 2016
* Description  : Set the Opportunity Payor code and PersonContactId on Oppty
/****************************************************************************/
    //Jagan - Kept below 3 lines because of Test classes dependencies
    public static List<Opportunity> toInsert;
    public static void processMaps(Set<Id> payorId, Set<Id> acctId){}
    public static void processOpportunity(Id payorId, Id opptyId, Opportunity opp, Id acctId){}
    
    public static void beforeInsert(List<Opportunity> oppList){
        
        Set<ID> acctIDs = New Set<ID>();
        Map<ID,ID> personids = New Map<ID,ID>();  
        //Jagan 10/4/2017 - Added below line
        Map<Id, String> accountIdToPayorCodeMap = new Map<Id, String>();
        /***Added By Abhishek on 11/18/2016********/    
        for(Opportunity opp : oppList){
            acctIDs.add(opp.accountID);
        }
        //Jagan 10/4/2017 - Added field Consumer_Payor_Code__c to the SOQL
        for(Account a : [Select OwnerID,PersonContactId,Consumer_Payor_Code__c From Account Where ID IN :acctIDs]){
            personids.put(a.id,a.PersonContactId);
            accountIdToPayorCodeMap.put(a.Id, a.Consumer_Payor_Code__c);
        }
        /****************************************/  
        if(UserInfo.getName() != 'Actian DataIntegrator'){
            for(Opportunity opp : oppList){
                acctIDs.add(opp.accountID);
            }
        }
        /***Added By Abhishek on 11/18/2016********/      
        for(Opportunity opp : oppList){
            if(opp.AccountId != null){
                ID PersonContactID  = personids.get(OPP.AccountID);    
                opp.PersonContact2__c = PersonContactID; 
                //Jagan 10/4/2017 - Added below line
                opp.Payor_Code__c = accountIdToPayorCodeMap.get(opp.AccountId);
            }    
            system.debug('##AP#PersonContactID'+opp.PersonContact2__c);
        }
        /*****************************************/   
        updateConnectionNameOnOpty(oppList);    //Priyanka Kajawe : PatnerNetworkRecordConnection 
    }
    
    /*************************************************************************************
* Author       : Noy De Goma@CSHERPAS
* Date         : 01.07.2016
* Description  : Responsible for creating opportunity team members upon 
*                creation depending on the Territory Name assigned to the Opportunity
/**************************************************************************************/
    
    //After Inserting Opportunity add relevant team members
    public static void afterInsert(List<Opportunity> oppList){
        List <Opportunity> oppWithTerritoryList = new List <Opportunity>();
        for(Opportunity opp : oppList){
            if(!String.isEmpty(opp.Territory_Name__c)){
                oppWithTerritoryList.add(opp);
            }
        }
        createOpportunityTeam(oppWithTerritoryList);
    }
    
    private static void createOpportunityTeam(list <Opportunity> opportunityList){
        list <String> territoryCodeList = new list <String>();
        List<OpportunityTeamMember> lstOppTeams = new List<OpportunityTeamMember>();
        map <String, Territory_Alignment__c> territoryMap = new map <String, Territory_Alignment__c>();
        Territory_Opportunity_Team_Role__c terrCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        for (Opportunity o : opportunityList){
            territoryCodeList.add(o.Territory_Name__c);
        }
        
        for (Territory_Alignment__c ta : [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                          FROM Territory_Alignment__c WHERE Territory_Code__c IN: territoryCodeList]){
                                              territoryMap.put(ta.Territory_Code__c, ta);
                                          }
        
        for (Opportunity o : opportunityList){
            if(territoryMap.get(o.Territory_Name__c) != null){
                if(territoryMap.get(o.Territory_Name__c).SA__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.SA__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).SA__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).PCS__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.PCS__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).PCS__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).Admin__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.Admin__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).Admin__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).MCS__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.MCS__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).MCS__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).DBM__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.DBM__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).DBM__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).RSD__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.RSD__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).RSD__c);
                    lstOppTeams.add(otm);
                }
                if(territoryMap.get(o.Territory_Name__c).Territory_Rep__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember(
                        TeamMemberRole = terrCS.Territory_Rep__c, 
                        OpportunityId  = o.Id,
                        UserId         = territoryMap.get(o.Territory_Name__c).Territory_Rep__c);
                    lstOppTeams.add(otm);
                }
            }
            
        }
        Database.SaveResult[] results = Database.insert(lstOppTeams,false);
        if (results != null){
            for (Database.SaveResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] errs = result.getErrors();
                    for(Database.Error err : errs){
                        System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
    }
    
    /****************************************************************************
* Author       : Jagan Periyakaruppan
* Date         : Oct 4 2017
* Description  : Sets the Payor code from Customer upon Opportunity closure
/****************************************************************************/
    public static void beforeUpdate (List<Opportunity> oppList)
    {
        //Prepare a map between accountid and its payor code
        Set<Id> acctIDs = New Set<Id>();
        Map<Id, String> accountIdToPayorCodeMap = new Map<Id, String>();
        for(Opportunity opp : oppList){
            if(opp.isClosed)
            {
                acctIDs.add(opp.accountID);
            }
        }
        if(!acctIDs.isEmpty())
        {
            for(Account a : [Select Consumer_Payor_Code__c From Account Where Id IN :acctIDs]){
                accountIdToPayorCodeMap.put(a.Id, a.Consumer_Payor_Code__c);
            }
            //Check if the Opportunity is set to closed
            for(Opportunity opp : oppList)
            {
                if(opp.isClosed)
                {
                    opp.Payor_Code__c = accountIdToPayorCodeMap.get(opp.AccountId);
                }
            }
        }  
        /*****************************/
        updateConnectionNameOnOpty(oppList);    //Priyanka Kajawe : PatnerNetworkRecordConnection 
    }
    
    /*************************************************************************************************
* Author        : Jagan Periyakaruppan
* Date          : 1/19/2017
* Description   : This method will update Medicare field values from Opportunity to Account
**************************************************************************************************/    
    public static void afterUpdate(List<Opportunity> oppList, Map<Id, Opportunity> oldMap){
		/*
        List<Account> accountsToBeUpdated = new List<Account>();
        Map<Id, Opportunity> accountToOppMap = new Map<Id, Opportunity>();
        for(Opportunity Opp : OppList)
        {
            if(Opp.RecordtypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId() &&
               (Opp.Last_MDCR_Reorder_Follow_up_Date__c != oldMap.get(Opp.Id).Last_MDCR_Reorder_Follow_up_Date__c ||
                Opp.Last_MDCR_Reorder_Follow_up_Type__c != oldMap.get(Opp.Id).Last_MDCR_Reorder_Follow_up_Type__c  ||
                Opp.MDCR_Communication_Preference__c != oldMap.get(Opp.Id).MDCR_Communication_Preference__c  ||
                Opp.First_MDCR_Order_Non_Direct__c != oldMap.get(Opp.Id).First_MDCR_Order_Non_Direct__c  ||
                Opp.First_MDCR_Order_Non_Direct_Channel__c != oldMap.get(Opp.Id).First_MDCR_Order_Non_Direct_Channel__c))
            {
                accountToOppMap.put(Opp.AccountID, Opp);
            }
        }
        
        if(!accountToOppMap.isEmpty()){
            List<Account> accList = [SELECT Id, Last_MDCR_Reorder_Follow_up_Date__c, Last_MDCR_Reorder_Follow_up_Type__c, MDCR_Communication_Preference__c, First_MDCR_Order_Non_Direct__c,  First_MDCR_Order_Non_Direct_Channel__c FROM Account WHERE Id IN: accountToOppMap.keySet()];
            for(Account acc : accList){
                acc.Last_MDCR_Reorder_Follow_up_Date__c = accountToOppMap.get(acc.Id).Last_MDCR_Reorder_Follow_up_Date__c;
                acc.Last_MDCR_Reorder_Follow_up_Type__c = accountToOppMap.get(acc.Id).Last_MDCR_Reorder_Follow_up_Type__c;
                acc.MDCR_Communication_Preference__c = accountToOppMap.get(acc.Id).MDCR_Communication_Preference__c;
                acc.First_MDCR_Order_Non_Direct__c = accountToOppMap.get(acc.Id).First_MDCR_Order_Non_Direct__c;
                acc.First_MDCR_Order_Non_Direct_Channel__c = accountToOppMap.get(acc.Id).First_MDCR_Order_Non_Direct_Channel__c;
                accountsToBeUpdated.add(acc);
            } 
        }  
        if(!accountsToBeUpdated.isEmpty()){
            update accountsToBeUpdated;
        }*/
    }
    
    /*************************************************************************************************
* Author        : Jagan Periyakaruppan
* Date          : 10/19/2017
* Description   : This method will be invoked to update the Copay on Pricebook update on Opportunity
**************************************************************************************************/
    
    public static void calculateCopayOnUpdate (List<Opportunity> opptyList, Map<Id, Opportunity> oldMap)
    {
        //Initialize collections to be worked on
        Set<Id> accountsToBeProcessed = new Set<Id>();
        Map<Id, Opportunity>  oppMap = new Map<Id, Opportunity>();
        Map<Id, Benefits__c> accountIdToBenefitMap = new Map <Id, Benefits__c>();
        Map<Id, Benefits__c> oppIdToBenefitMap = new Map <Id, Benefits__c>();
        
        //Filter the Opportunity records, which are to be processed
        for(Opportunity opp : opptyList)
        {
            if((opp.Pricebook2Id != oldMap.get(opp.Id).Pricebook2Id) || (opp.Admin_Flag__c == true))
            {
                if(opp.Pricebook2Id == null)
                {
                    opp.Estimated_Cost__c = null;
                }
                else
                {
                    oppMap.put(opp.Id, opp);
                    accountsToBeProcessed.add(opp.AccountId);
                }
            }
        }
        //Proceed only when Opportunity exists for processing copay logic
        if(!oppMap.isEmpty())
        {
            //Get all primary benefit details connected to Opportunity Account records  
            for(Benefits__c benefit : [SELECT Account__c, Coverage__c, CO_PAY__c, Copay_Line_Item__c, INDIVIDUAL_DEDUCTIBLE__c, INDIVIDUAL_MET__c, INDIVIDUAL_OOP_MAX__c, INDIVIDUAL_OOP_MET__c, FAMILY_DEDUCT__c, Family_Met__c, FAMILY_OOP_MAX__c, FAMILY_OOP_MET__c, Last_Benefits_Check_Date__c, PRIOR_AUTH_REQUIRED__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary' AND Account__c IN :accountsToBeProcessed])
            {
                accountIdToBenefitMap.put(benefit.Account__c, benefit);
            }
            //Prepare Opportunity to Benefit records map
            for(Opportunity opp : oppMap.values())
            {
                oppIdToBenefitMap.put(opp.Id, accountIdToBenefitMap.get(opp.AccountId));
            }
            //Invoke the copay calculation
            if(!oppIdToBenefitMap.isEmpty())
            {
                ClsCopayCalculation.ProcessOpportunitiesForCopayCalculation (oppMap, oppIdToBenefitMap);
            }
        }  
    }
    /*************************************************************************************************
* Author        : Priyanka Kajawe
* Date          : 14/05/2018
* Description   : This method is for updating connection name on opty
**************************************************************************************************/
    public static void updateConnectionNameOnOpty (List<Opportunity> opptyList){
        Set<Id> OpptyRecIds = new Set<Id>();
        Map<Id,PartnerNetworkRecordConnection> mapPartnerConn = new Map<Id, PartnerNetworkRecordConnection>();
        
        for(Opportunity opp: opptyList){
            OpptyRecIds.add(opp.Id);
        }  
        
        List<PartnerNetworkRecordConnection> recordConns = new List<PartnerNetworkRecordConnection>([select Id, Status, ConnectionId, LocalRecordId from PartnerNetworkRecordConnection where LocalRecordId in :OpptyRecIds]);
        If(recordConns.size()>0)
        {
            for(PartnerNetworkRecordConnection recordConn : recordConns)
            {  
                mapPartnerConn.put(recordConn.LocalRecordId, recordConn);
            }
            
            for(Opportunity Opp: opptyList){
                if(mapPartnerConn.keySet().contains(Opp.id)  && 
                   (mapPartnerConn.get(Opp.id).Status.equalsignorecase('sent') || 
                    mapPartnerConn.get(Opp.id).Status.equalsignorecase('pending (sent)')))
                { 
                    if(Opp.Received_Connection_Name__c == NULL){
                        if(mapPartnerConn.get(Opp.Id).ConnectionId == '04P400000008trVEAQ'){
                            Opp.Received_Connection_Name__c = 'DSC';
                        }
                        else
                        {
                            Opp.Received_Connection_Name__c = 'SOL';
                        }
                    }
                }
                if(mapPartnerConn.keySet().contains(Opp.id)  && 
                   (mapPartnerConn.get(Opp.id).Status.equalsignorecase('Inactive') || 
                    mapPartnerConn.get(Opp.id).Status.equalsignorecase('deleted') || 
                    mapPartnerConn.get(Opp.id).Status.equalsignorecase('converted'))){
                    if(Opp.Received_Connection_Name__c != NULL){
                        Opp.Received_Connection_Name__c = '';
                    }
                }
            }   
        }              
    }
}