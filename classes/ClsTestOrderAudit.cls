/*************************************************************************************************
Apex Class Name : OrderAudit_Test
Version : 0.1
Created Date :  13 Dec, 2018
Function :  This is a Test Class for OrderAudit Apex Class.
---------------------------------------------------------------------------------------------------
* Developer  LTI                     Date 13 Dec, 2018                           	
**************************************************************************************************/
@isTest
public class ClsTestOrderAudit {
    // This Method Creates all Records used in Other test methods of this class
    @testSetup static void setup() {
        Id DesalesorderRTId;
        Id DeconsumerRTId;
        Id DepayorRTId;
        Id DemedicalRTId;
        Id DeprescriberRTId;
        list<recordtype> lstrt1 =  [Select Id,developername
                         from RecordType 
                        where developername 
                  in ('DE_Sales_Order','DE_Consumer','DE_Payor','DE_Medical_Facility','DE_Prescriber')];
		if(lstrt1.size() > 0){
            for(recordtype r: lstrt1){
                if(r.developername == 'DE_Sales_Order'){
                   DesalesorderRTId = r.id; 
                } else if(r.developername == 'DE_Consumer'){
                   DeconsumerRTId = r.id; 
                } if(r.developername == 'DE_Payor'){
                   DepayorRTId = r.id; 
                } if(r.developername == 'DE_Medical_Facility'){
                   DemedicalRTId = r.id; 
                } if(r.developername == 'DE_Prescriber'){
                   DeprescriberRTId = r.id; 
                }
            }
        }
             
        account oacc1=new account();
        oacc1.firstname='test';
        oacc1.lastname='account1';
        if(DeconsumerRTId != null){
            oacc1.recordtypeid=DeconsumerRTId;
        }    
        oacc1.Billingstreet = 'bill street 1';
        oacc1.Billingcity = 'bill city 1';
        oacc1.Billingcountry = 'germany';
        oacc1.Billingpostalcode = '11111';
        oacc1.shippingstreet = 'ship street 1';
        oacc1.shippingcity = 'ship city 1';
        oacc1.shippingcountry = 'germany';
        oacc1.shippingpostalcode = '22222';
        
        account oacc2= new account();
        oacc2.name='test account2';
        if(DepayorRTId != null){
            oacc2.recordtypeid=DepayorRTId;
        }    
        oacc2.Insurance_Company_Identification_Number__c='111111';
        oacc2.Billingstreet = 'bill street2';
        oacc2.Billingcity = 'bill city2';
        oacc2.Billingcountry = 'germany';
        oacc2.Billingpostalcode = '33333';
        oacc2.shippingstreet = 'ship street2';
        oacc2.shippingcity = 'ship city2';
        oacc2.shippingcountry = 'germany';
        oacc2.shippingpostalcode = '44444';
        
        account oacc3= new account();
        oacc3.name='test account3';
        if(DemedicalRTId != null){
            oacc3.recordtypeid=DemedicalRTId;
        }    
        oacc3.Billingstreet = 'bill street3';
        oacc3.Billingcity = 'bill city3';
        oacc3.Billingcountry = 'germany';
        oacc3.Billingpostalcode = '55555';
        oacc3.shippingstreet = 'ship street3';
        oacc3.shippingcity = 'ship city3';
        oacc3.shippingcountry = 'germany';
        oacc3.shippingpostalcode = '66666';
        
        account oacc4= new account();
        oacc4.firstname='test';
        oacc4.lastname='account4';
        oacc4.Prescriber_Id__c='1111';
        if(DemedicalRTId != null){
            oacc4.recordtypeid=DeprescriberRTId;
        }    
        oacc4.Billingstreet = 'bill street4';
        oacc4.Billingcity = 'bill city4';
        oacc4.Billingcountry = 'germany';
        oacc4.Billingpostalcode = '77777';
        oacc4.shippingstreet = 'ship street4';
        oacc4.shippingcity = 'ship city4';
        oacc4.shippingcountry = 'germany';
        oacc4.shippingpostalcode = '88888';
        insert new list<account>{oacc1,oacc2,oacc3,oacc4};              
        if(oacc1 != null && oacc2 != null && oacc3 != null && oacc4 != null){
        	Address__c oaddr1= new Address__c();
            oaddr1.Account__c=oacc1.id;
            oaddr1.type__c='bill to';
            oaddr1.Address_Line_1__c = '1 address line 1';
            oaddr1.City__c = 'test city1';
            oaddr1.Postal_Code__c = '11111';
            oaddr1.Country__c = 'germany';
            
            Address__c oaddr2= new Address__c();
            oaddr2.Account__c=oacc1.Id;
            oaddr2.type__c='ship to';
            oaddr2.Address_Line_1__c = '2 address line 1';
            oaddr2.City__c = 'test city2';
            oaddr2.Postal_Code__c = '22222';
            oaddr2.Country__c = 'germany';
            
            Address__c oaddr3= new Address__c();
            oaddr3.Account__c=oacc2.id;
            oaddr3.type__c='bill to';
            oaddr3.Address_Line_1__c = '3 address line 1';
            oaddr3.City__c = 'test city3';
            oaddr3.Postal_Code__c = '33333';
            oaddr3.Country__c = 'germany';
            oaddr3.Inactive__c=false;
            insert new list<address__c>{oaddr1,oaddr2,oaddr3};
        
            pricebook2 opri1 = new pricebook2();
            opri1.name = 'orderaudit pricebook1';
            opri1.isactive=true;
            insert opri1;
            
            if(opri1 != null){
                opportunity oopp1 = new opportunity();
                oopp1.name='orderAudit  opportunity';
                oopp1.StageName='new opportunity';
                oopp1.CloseDate=system.today();
                oopp1.accountid=oacc1.id;
                oopp1.Medical_Facility__c=oacc3.id;
                oopp1.Prescriber__c=oacc4.id;
                oopp1.Payor__c=oacc2.id;
                oopp1.Pricebook2id=opri1.id;
                insert oopp1;
                
                order oord1 = new Order();
                if(lstrt1.size() > 0 && oopp1 != null){
                    if(DesalesorderRTId != null){
                        oord1.recordtypeid = DesalesorderRTId;    
                    }
                    oord1.accountid=oacc1.id;
                    oord1.status = 'Draft';
                    oord1.Type='de standard';
                    oord1.Customer_Ship_To_Address__c=oaddr2.id;
                    oord1.EffectiveDate = system.today();
                    oord1.Customer_Bill_To_Address__c = oaddr1.id;
                    oord1.Payor__c=oacc2.id;
                    oord1.Payor_Bill_To_Address__c=oaddr3.id;
                    oord1.OpportunityId=oopp1.id; 
                    oord1.Price_Book__c=opri1.id;
                    insert oord1;
                    
                    product2 oprod1 = new product2();
                    oprod1.name = 'test product';
                    oprod1.isactive=true;
                    insert oprod1;
                    
                    if(oprod1 != null && oord1 != null){
                        PricebookEntry opre1 = new PricebookEntry();
                        opre1.isactive=true;
                        Id pricebookId = Test.getStandardPricebookId();
                        opre1.Pricebook2Id=pricebookId;
                        opre1.Product2Id=oprod1.id;
                        opre1.UnitPrice=1;
                        opre1.UseStandardPrice=false;
                        insert opre1;
                            
                        pricebookentry opre2 = new pricebookentry();
                        opre2.isactive=true;
                        opre2.Pricebook2Id=opri1.id;
                        opre2.Product2Id=oprod1.id;
                        opre2.UnitPrice=1;
                        opre2.UseStandardPrice=false;
                        insert opre2;
 						                   
                        if(opre2 != null){
                            OrderItem ooi1 = new OrderItem();
                            ooi1.orderid=oord1.id;
                            ooi1.PricebookEntryId=opre2.id;
                            ooi1.product2id=oprod1.id;
                            ooi1.Quantity=1;
                            ooi1.UnitPrice=1;
                            insert ooi1;
                        }    
                    }    
                }    
            } 
        }
    }    
    // This Test Method executes all Methods of OrderAudit Apex Class
    static testMethod void validatemethods(){
		test.startTest();
        list<order> lstOrd1 = [select id,status,shipping_hold_status__c from order limit 1];
        string result = '';
        list<Audit_Tracker__c> lstaudit1 = new list<Audit_Tracker__c> ();
        Map<string,list<Audit_Tracker__c>> maudit1 = new Map<string,list<Audit_Tracker__c>>();
        CtrlOrderAudit.wrapperClass wrap1 = new CtrlOrderAudit.wrapperClass ();    
        order oord1 = new order();
        if(lstOrd1.size() > 0){
            //Coverage for Exception Handling
            result = CtrlOrderAudit.updateorder('draft',oord1);
            system.assert(result.contains('Error'));
            wrap1= CtrlOrderAudit.getaccessibility(lstord1[0].id);
            system.assertEquals(true, wrap1.hasAccess);
            system.assertEquals(false, wrap1.iscomplete);
            system.assertEquals(false, wrap1.isEditable);
            system.assertEquals('Not Started', wrap1.Shipholdstatus);
            // Update Order status to Shipping Hold 
            lstord1[0].status='shipping hold';
            update lstord1;
            
            wrap1= CtrlOrderAudit.getaccessibility(lstord1[0].id);
            maudit1 = CtrlOrderAudit.inithelpermethod(lstord1[0].id);
            lstaudit1 = [select id,Audit_Field_Name__c,Field_Verified__c,Object_id__c 
                                                from Audit_Tracker__c where object_id__c = :lstord1[0].id];
            if(lstaudit1.size() > 0){
                //Coverage for Exception Handling
            	result = CtrlOrderAudit.submitaudit('',lstaudit1); 
                system.assert(result.contains('Error'));
            
                result = CtrlOrderAudit.saveaudit(lstaudit1);    
                system.assert(result.contains('success'));
            
                result = CtrlOrderAudit.submitaudit(lstord1[0].id,lstaudit1);  
                // Completing Audit records Verification
                for(Audit_Tracker__c at: lstaudit1){
                    at.Field_Verified__c = true; 
                }
                update lstaudit1;
                result = CtrlOrderAudit.submitaudit(lstord1[0].id,lstaudit1);
                
          		wrap1 = CtrlOrderAudit.getaccessibility(lstord1[0].id);
                // Update Order Status to Draft
                order oorder1=[select id,status,shipping_hold_status__c
                          from order
                          where Id= :lstord1[0].id];
                system.assertEquals('Completed', oorder1.shipping_hold_status__c);
                system.assertEquals('Activated', oorder1.status);
                oorder1.Status='draft';
                update oorder1;
                // Delete Order Product
                orderitem oitem1=[select id
                          from orderitem
                          where OrderId= :lstord1[0].id];
                delete oitem1;
                // Delete Audit Records
                delete lstaudit1;
                //Coverage for Exception Handling
            	maudit1 = CtrlOrderAudit.inithelpermethod(lstord1[0].id);
                result = CtrlOrderAudit.saveaudit(lstaudit1);
                system.assert(result.contains('error'));

            }
        }
        test.stopTest();                   
    }
}