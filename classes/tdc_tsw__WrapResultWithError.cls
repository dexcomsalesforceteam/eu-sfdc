/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WrapResultWithError implements tdc_tsw.ConversationViewInterface {
    @AuraEnabled
    global String AllUserView;
    @AuraEnabled
    global String defaultphnApi;
    @AuraEnabled
    global String errorMsg;
    @AuraEnabled
    global Boolean isEndChat;
    @AuraEnabled
    global Boolean isError;
    @AuraEnabled
    global Boolean isMMSEnabled;
    @AuraEnabled
    global Boolean isNew;
    @AuraEnabled
    global String lastRefreshedTime;
    @AuraEnabled
    global String loggedUserView;
    @AuraEnabled
    global String loginUserId;
    @AuraEnabled
    global String loginUserName;
    @AuraEnabled
    global String loginUserPicUrl;
    @AuraEnabled
    global List<tdc_tsw.WrapMSGwithTime> lstAllMsg;
    @AuraEnabled
    global List<User> lstUser;
    @AuraEnabled
    global tdc_tsw.WrapSendSMSComponents objData;
    global tdc_tsw.WrapObjectVariables objVariables;
    @AuraEnabled
    global tdc_tsw.WrapDataVariables objWrapDataVar;
    @AuraEnabled
    global String OrganizationType;
    @AuraEnabled
    global String phnApiJson;
    @AuraEnabled
    global String toNumber;
    @AuraEnabled
    global Integer unreadIncomings;
    @AuraEnabled
    global List<String> unreadMsgIds;
    @AuraEnabled
    global String userJson;
    global WrapResultWithError() {

    }
}
