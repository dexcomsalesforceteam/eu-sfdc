/********************************************************
**Description: Account trigger handler Class. All the CRUD events are handled here.
**Author:      Louis Augusto Del Rosario, CLOUD SHERPAS
**Date Created:    JULY.14.2015
*******************************************************
**@Author Priyanka Kajawe
**@Reason Merged trigger Account_Update_Opportunity

*********************************************************/

public class AccountTriggerHandler {
    public static void onBeforeInsert(List<Account> listAccount){
        copyEmail(listAccount);
    }
    
/********************************************************************************
* revision     
Kingsley Tumaneng NOV 3, 2015 - Added a validation if fields
are changed, it will update the Integration Last Modified Date

002 BHU 11/21/2014 - Changed to Billing State from 
 State when assigning Medicare pricebooks

*********************************************************************************/
    public static void onBeforeUpdate(List<Account> listAccount, Map<Id, Account> oldMap){
        List<Account> toBeUpdated = new List<Account>();
        
        Id RecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        
        for(Account acc : listAccount){
            if(acc.Name != oldMap.get(acc.Id).Name || 
               acc.Gender__c != oldMap.get(acc.Id).Gender__c || 
               acc.DOB__c != oldMap.get(acc.Id).DOB__c ||  
               acc.Payor__c != oldMap.get(acc.Id).Payor__c ||  
               acc.PersonMobilePhone != oldMap.get(acc.Id).PersonMobilePhone ||  
               acc.PersonHomePhone != oldMap.get(acc.Id).PersonHomePhone || 
               acc.PersonOtherPhone != oldMap.get(acc.Id).PersonOtherPhone ||
               acc.PersonEmail != oldMap.get(acc.Id).PersonEmail ||
               acc.Medical_Facility__c != oldMap.get(acc.Id).Medical_Facility__c ||
               acc.Prescribers__c != oldMap.get(acc.Id).Prescribers__c ||
               acc.Diagnosis_Code_1_Acc__c != oldMap.get(acc.Id).Diagnosis_Code_1_Acc__c ||
               acc.Diagnosis_Code_2_Acc__c != oldMap.get(acc.Id).Diagnosis_Code_2_Acc__c ||
               acc.CMN_or_Rx_Expiration_Date__c != oldMap.get(acc.Id).CMN_or_Rx_Expiration_Date__c ||
               acc.Preferred_Language__c != oldMap.get(acc.Id).Preferred_Language__c ||
               acc.Inactive__c != oldMap.get(acc.Id).Inactive__c|| 
               acc.Integrated_Pump_Partner_new__c != oldMap.get(acc.Id).Integrated_Pump_Partner_new__c||
               acc.Customer_Type__c != oldmap.get(acc.id).Customer_Type__c ||
               acc.Default_Price_Book__c != oldMap.get(acc.Id).Default_Price_Book__c){
                   acc.Integration_Last_Modified_Date__c = dateTime.now();
                   toBeUpdated.add(acc);
               }
        }
        //}
        
        copyEmail(listAccount);
    }
    /**********************************************************
**Description: Copys the person email to the custom person email field
**Parameters:  List<Account> listAccount
**Returns:     None
**Author:      CLOUD SHERPAS, Louis Augusto Del Rosario
**Date Created:    JULY.14.2015
**********************************************************/

    /*Description: Method copies email address
    *Param: List<Account>
    *Return: void
    */
    
    public static void copyEmail(List<Account> listAccount){
        for(Account a:listAccount){
            a.Personemail__c = a.PersonEmail;
        }
    }
    /************************************************************************************************
* @author      : Jagan Periyakaruppan
* @date        : Oct 03 2017
* @description : Added process logic to set Default pricebook on
Account upon customer type change to 'Medicare FFS' or 'Medicare Advantage'
************************************************************************************************/

    /*Description: Method executes when a customer type is populated on account and it happens to be any one of the Medicare values
    *Param: List<Account>
    *Return: void
    */
    public static void updateDefaultPricebookOnInsert(List<Account> listAccount){
        for(Account acc : listAccount)
        {
            if(acc.Customer_Type__c != null)
            {
                Map<String, String> medicarePricebookMapping = getMedicarePricebookByStateMapping();
                if(acc.Customer_Type__c == 'Medicare FFS')
                {
                    // BHU 002
                    //String pricebookId = medicarePricebookMapping.get(acc.ShippingState);
                    String pricebookId = medicarePricebookMapping.get(acc.BillingState);
                    // END BHU 002
                    acc.Default_Price_Book__c = pricebookId;
                }
            }   
        }
        
    }
    /*Description: Method executes when a customer type is changed on account and it happens to be any one of the Medicare values
    *Param: List<Account>, Map<Id, Account>
    *Return: void
    */
    public static void updateDefaultPricebookOnUpdate(List<Account> listAccount, Map<Id, Account> oldMap){
        
        //Boolean to check if we can proceed to update the default pricebook on the Account
        Boolean canProceedWithPBUpdate = false;
        Set<Id> payorIds = new Set<Id>();
        Set<Id> pricebookIds = new Set<Id>();
        for(Account acc : listAccount)
        {
            //Find the customers that are to be processed
            if(acc.Customer_Type__c != null)
            {
                //Check if the customer type value is changed
                if(acc.Customer_Type__c != oldMap.get(acc.Id).Customer_Type__c)
                {
                    canProceedWithPBUpdate = true;
                    //Collect all the Payor Ids to get the Payor information
                    if(acc.Payor__c != null)
                        payorIds.add(acc.Payor__c);
                    //Collect all the Pricebook information tied to the accounts that are to be updated
                    if(acc.Default_Price_Book__c != null)
                        pricebookIds.add(acc.Default_Price_Book__c);
                }   
            }
        }
        
        if(canProceedWithPBUpdate)
        {
            Map<Id, Account> payorInfo = getPayorInfo(payorIds);
            Map<Id, Boolean> cashPBCheckMap = getPricebookMapForCashPriceCheck(pricebookIds);
            Map<String, String> medicarePricebookMapping = getMedicarePricebookByStateMapping();
            for(Account acc : listAccount)
            {
                if(acc.Customer_Type__c != null)
                {
                    //Check if the customer type value is changed
                    if(acc.Customer_Type__c != oldMap.get(acc.Id).Customer_Type__c)
                    {
                        //Execute below If block if the customer type is 'Medicare FFS'
                        if(acc.Customer_Type__c == 'Medicare FFS')
                        {
                            // BHU 002
                            //String pricebookId = medicarePricebookMapping.get(acc.ShippingState);
                            String pricebookId = medicarePricebookMapping.get(acc.BillingState);
                            // END BHU 002
                            acc.Default_Price_Book__c = pricebookId;
                        }
                        //Execute below If block if the customer type is 'Medicare Advantage'
                        else if (acc.Customer_Type__c == 'Medicare Advantage')
                        {
                            //Check if Payor exist for the customer and then proceed
                            if(acc.Payor__c != null)
                            {   
                                Account payor = payorInfo.get(acc.Payor__c);
                                if(payor.Payor_Code__c == 'K')
                                    acc.Default_Price_Book__c = payor.Default_Price_Book_K__c;
                                else if(payor.Payor_Code__c == 'A')
                                    acc.Default_Price_Book__c = payor.Default_Price_Book_A__c;
                            }   
                        }
                        //Remove the pricebook if it is not cash pricebook. Also, do not alter the pricebook if the customer type was null before
                        else if(acc.Default_Price_Book__c != null && oldMap.get(acc.Id).Customer_Type__c != null)
                        {
                            acc.Default_Price_Book__c = cashPBCheckMap.get(acc.Default_Price_Book__c) == TRUE ? acc.Default_Price_Book__c : NULL; 
                        }
                    }
                }   
            }
        }   
    }
    /*Description: Method retrieves the payor information
    *Param: Set<Id>
    *Return: Map<Id, Account>
    */
    public static Map<Id, Account> getPayorInfo (Set<Id> payorIds)
    {
        Map<Id, Account> payorMap = new Map<Id, Account>();
        Id payorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        for(Account payor : [SELECT Id, Name, Default_Price_Book_A__c, Default_Price_Book_K__c, Payor_Code__c FROM Account WHERE RecordtypeId = :payorRecordTypeId AND Id IN : payorIds])
        {
            payorMap.put(payor.Id, payor);
        }
        return payorMap;
    }
    
    /*Description: Method retrieves the Pricebook information
    *Param: Set<Id>
    *Return: Map<Id, Boolean>
    */
    public static Map<Id, Boolean> getPricebookMapForCashPriceCheck (Set<Id> pricebookIds)
    {
        Map<Id, Boolean> pbIdToCashPayFlagMap = new Map<Id, Boolean>();
        for(Pricebook2 pb : [SELECT Id, Cash_Price_Book__c FROM Pricebook2 Where Id IN : pricebookIds])
        {
            pbIdToCashPayFlagMap.put(pb.Id, pb.Cash_Price_Book__c);
        }
        return pbIdToCashPayFlagMap;
    }
    
    /*Description: Jagan 10/12/2017 - Returns map between the state and pricelist. This will be used in AccountTriggerHandler
    *Param: N/A
    *Return: Map<String, String>
    */
    public static Map<String, String> getMedicarePricebookByStateMapping()
    {
        Map<String, String> mdcrPricebookNameByState = new Map<String, String>();
        Map<String, String> pricebookNameToId = new Map<String, String>();
        Map<String, String> mdcrPricebookIdByState = new Map<String, String>();
        
        //Instantiate the custom metadata type 'MDCR Pricebook Mapping' to get all its values. Prepare a map between the BillingState and the Medicare Pricebook name
        MDCR_Pricebook_Mapping__mdt[] mdcrPricebookMappingList = [SELECT DeveloperName, State__c, Pricebook_Name__c FROM MDCR_Pricebook_Mapping__mdt];
        for(MDCR_Pricebook_Mapping__mdt mdcrPricebookMapping : mdcrPricebookMappingList)
        {
            mdcrPricebookNameByState.put(mdcrPricebookMapping.State__c, mdcrPricebookMapping.Pricebook_Name__c);
        }
        
        //Prepare a map between Medicare Pricebook Name and Medicare Pricebook Id
        for(Pricebook2 mdcrPricebook : [SELECT Id, Name FROM Pricebook2 WHERE Name IN : mdcrPricebookNameByState.values()])
        {
            pricebookNameToId.put(mdcrPricebook.Name, mdcrPricebook.Id);
        }
        
        //Prepare a map between a state value and the Medicare Pricebook Id
        for(String stateVal : mdcrPricebookNameByState.keySet())
        {
            mdcrPricebookIdByState.put(stateVal, pricebookNameToId.get(mdcrPricebookNameByState.get(stateVal)));
        }
        
        return mdcrPricebookIdByState;
    }   
    
    /*Description: Method executes after account update to process any open MFRs for the customer type changes on Account
    *Param: List<Account>, Map<Id, Account> 
    *Return: void
    */
    public static void updateOpenMFRs(List<Account> listAccount, Map<Id, Account> oldMap){
        Set<Id> accountsWithOpenMFRsSet = new Set<Id>();
        
        for(Account acc : listAccount)
        {
            //If there is change in customer type and if the account has open MFRs then they need to be updated
            if((acc.Customer_Type__c != oldMap.get(acc.Id).Customer_Type__c) && acc.Num_Of_Open_MDCR_Followup_Records__c > 0)
                accountsWithOpenMFRsSet.add(acc.Id);
        }
        //Process any open MFRs
        if(!accountsWithOpenMFRsSet.isEmpty())
        {
            //List of MFRs to be updated
            List<MDCR_Followup__c> mfrsTOBeUpdated = new List<MDCR_Followup__c>([SELECT Id, Customer__c, MDCR_Followup_Status__c, MDCR_Price_Book__c, Customer__r.Payor__r.Payor_Code__c, Customer__r.Payor__c, Customer__r.Payor__r.Default_Reorder_Price_Book__c, Customer__r.Default_Price_Book__c, Customer__r.Customer_Type__c FROM MDCR_Followup__c WHERE MDCR_Followup_Status__c != 'Closed' AND MDCR_Followup_Status__c != 'Cancelled' AND Customer__c IN : accountsWithOpenMFRsSet]);
            
            if(!mfrsTOBeUpdated.isEmpty())
            {
                //Get Reorder Pricebook
                Id reorderPricebookId = [SELECT Id FROM Pricebook2 WHERE Name = 'M/C - Reorder Price List'].Id;
                for(MDCR_Followup__c mfr : mfrsTOBeUpdated)
                {
                    if(mfr.Customer__r.Customer_Type__c == 'Medicare FFS')
                        mfr.MDCR_Price_Book__c = reorderPricebookId;
                    else if(mfr.Customer__r.Customer_Type__c == 'Medicare Advantage' && mfr.Customer__r.Payor__r.Payor_Code__c == 'K')
                        mfr.MDCR_Price_Book__c = mfr.Customer__r.Payor__r.Default_Reorder_Price_Book__c;
                    else
                    {
                        mfr.MDCR_Price_Book__c = null;
                        mfr.MDCR_Followup_Status__c = 'Cancelled';
                    }   
                }
                try{
                    update mfrsTOBeUpdated;
                }catch(Exception e){
                    system.debug('***MFRs update Error = ' + e.getMessage());
                }
            }       
        }
        
    }
    /*Description: Method executes Copay calculation after pricebook update
    *Param: List<Account>, Map<Id, Account> 
    *Return: void
    */
    public static void calculateCopayOnUpdate(List<Account> listAccount, Map<Id, Account> oldMap){
        Set<Id> accountsToBeProcessed = new Set<Id>();
        Map<Id, Account>  accountMap = new Map<Id, Account>();
        Map<Id, Benefits__c> accountIdToBenefitMap = new Map <Id, Benefits__c>();
        
        for(Account acc : listAccount)
        {
            //If there is change in customer type and if the account has open MFRs then they need to be updated
            if((acc.Default_Price_Book__c != oldMap.get(acc.Id).Default_Price_Book__c))
            {
                if(acc.Default_Price_Book__c == null)
                {
                    acc.Estimated_Cost__c = null;
                }
                else
                {
                    accountMap.put(acc.Id, acc);
                    accountsToBeProcessed.add(acc.Id);
                }
            }
        }
        //Execute when there are accounts to be processed.      
        if(!accountMap.isEmpty())
        {
            for(Benefits__c benefit : [SELECT Account__c, Coverage__c, CO_PAY__c, Copay_Line_Item__c, INDIVIDUAL_DEDUCTIBLE__c, INDIVIDUAL_MET__c, INDIVIDUAL_OOP_MAX__c, INDIVIDUAL_OOP_MET__c, FAMILY_DEDUCT__c, Family_Met__c, FAMILY_OOP_MAX__c, FAMILY_OOP_MET__c, Last_Benefits_Check_Date__c, PRIOR_AUTH_REQUIRED__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary' AND Account__c IN :accountsToBeProcessed])
            {
                accountIdToBenefitMap.put(benefit.Account__c, benefit);
            }
            
            if(!accountIdToBenefitMap.isEmpty())
            {
                ClsCopayCalculation.ProcessAccountsForCopayCalculation (accountMap, accountIdToBenefitMap, 'Account');
            }   
        }   
    }
    
    /*Description: Method executes Copay calculation after pricebook update
    *Param: List<Account>, Map<Id, Account> 
    *Return: void
    */
    public static void UpdateOpportunities(List<Account> listAccount, Map<Id, Account> newMap, Map<Id, Account> oldMap){
    /***************************************************************
    //Summary: Update mobile phone in opporunity after account is updated.
    //Programmer: BHU
    //Change Log
    //  04/22/2015 BHU Initial Version
    //
    //-- Modified by Noy De Goma@CSHERPAS on 11.03.2015
    //--Added Prescribers__c and Medical_Facility__c to be updated
    /***************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 06/12/2017
    @Description    : Added logic to carry over Medicare Pricebook
    ***************************************************************/
    /***************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 10/03/2017
    @Description    : Added logic to handle Open opportunities 
    with products when the customer type changes 
    on account
    ***************************************************************
    @Author        : Andrew Akre
    @Date Modified    : 9/10/2018
    @Description    : Added and removed certain Territory Alignment fields grabbed from accounts
    ***************************************************************/
    
    //  Get the IDs of the Accounts
    Set<ID> accIDs = newMap.keySet();
    
    // Added by Jagan on 10.03.2017 - Capture the account Ids, which are affected with customer type
    Set<Id> accountsWithMedicareChangesSet = new Set<Id>();
    
    // Added by Jagan on 10.03.2017 - Capture the account Ids, which are affected with customer type
    Set<Id> openOpptyWithLineItemsToBeDeletedSet = new Set<Id>();
    
    // Added by Jagan on 10.03.2017 - Find the accounts, which have customer type changes from or to Medicare values
    for(Account acc : listAccount)
    {
        //Find if the customer type value has been changed 
        if(acc.Customer_Type__c != oldMap.get(acc.Id).Customer_Type__c)
        {
            system.debug('***********Code from Oppty trigger Account pricebook is ' + acc.Default_Price_Book__r.Cash_Price_Book__c);
            //If old or new Customer Type is Medicare, ignore cash pricebook
            if(acc.Customer_Type__c != null)
            {
                if(acc.Default_Price_Book__c != null)
                {
                    if(acc.Default_Price_Book__r.Cash_Price_Book__c == false)
                        accountsWithMedicareChangesSet.add(acc.Id);
                }
                else
                    accountsWithMedicareChangesSet.add(acc.Id);
            }
        }
    }
    //  Get all Opportunities associated with the Account in this Trigger
    List<Opportunity> OppoList = [
        SELECT Id, Account.Id, RecordtypeId, Account.PersonContact.MobilePhone, Pricebook2Id, prescribers__c, Medical_Facility__c, Mobile_Phone__c, Account.Payor__c, Account.Prescribers__c, Account.Medical_Facility__c, Account.Last_MDCR_Reorder_Follow_up_Type__c, First_MDCR_Order_Non_Direct__c, First_MDCR_Order_Non_Direct_Channel__c, Account.Last_MDCR_Reorder_Follow_up_Date__c, Account.MDCR_Communication_Preference__c, Account.Default_Price_Book__c, Account.First_MDCR_Order_Non_Direct__c,Account.First_MDCR_Order_Non_Direct_Channel__c,Account.Consumer_Payor_Code__c, Last_MDCR_Reorder_Follow_up_Type__c, Last_MDCR_Reorder_Follow_up_Date__c, MDCR_Communication_Preference__c, Count_Of_Oppty_Products__c, Payor_Code__c, Estimated_Cost__c, Account.Estimated_Cost__c, Opp_Territory_AS__c, Account.Territory_AS__c, Opp_Territory_Fax__c, Account.Territory_Fax__c, Opp_Territory_ISR_RingDNA__c, Account.Territory_ISR_RingDNA__c, Opp_Territory_ISR_Email__c, Account.Territory_ISR_Email__c, Opp_Territory_RSS_RingDNA__c, Account.Territory_RSS_RingDNA__c, Opp_Territory_RSS_Email__c, Account.Territory_RSS_Email__c, Opp_Territory_RSS__c, Account.Territory_RSS__c, Opp_Territory_Medicare_RSS__c, Account.Territory_Medicare_RSS__c, Opp_Territory_Medicare_RSS_RingDNA__c, Account.Territory_Medicare_RSS_RingDNA__c, Opp_Territory_Supervisor__c, Account.Territory_Supervisor__c, Opp_Territory_ID__c, Account.Territory_ID_Lookup__c//Jagan - added additional fields
        FROM Opportunity
        WHERE IsClosed = false
        AND (StageName != '61. Quote Approved' AND StageName != '10. Cancelled')
        AND Account.Id IN :accIDs ] ;
    
    //Opportunities to be updated
    List<Opportunity> oppsToBeUpdated = new List<Opportunity>();
    
    //  Loop through them to copy Number of Partners from parent (Account) to the Opportunity
    for(Opportunity opps: OppoList)
    {
        // Added by Anuj Patel on 10.20.2017
        if(opps.RecordtypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('EMEA CH Opportunity').getRecordTypeId() &&
            (
            opps.Account.Prescribers__c != oldMap.get(opps.AccountId).Prescribers__c ||
            opps.Account.Medical_Facility__c != oldMap.get(opps.AccountId).Medical_Facility__c
            )
        )
        {
        if(opps.Account.Prescribers__c != oldMap.get(opps.AccountId).Prescribers__c){
                opps.Prescribers__c = opps.Account.Prescribers__c;
            }
        if(opps.Account.Medical_Facility__c != oldMap.get(opps.AccountId).Medical_Facility__c){
                opps.Medical_Facility__c = opps.Account.Medical_Facility__c;
            }
    
       oppsToBeUpdated.add(opps); 
           
        }
        
        system.debug('***********Code from Oppty trigger Oppty pricebook is ' + opps.Pricebook2Id);
        if(opps.RecordtypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId() &&
            (opps.Account.Payor__c != oldMap.get(opps.AccountId).Payor__c ||
            opps.Mobile_Phone__c != oldMap.get(opps.AccountId).PersonContact.MobilePhone ||
            opps.Account.Prescribers__c != oldMap.get(opps.AccountId).Prescribers__c ||
            opps.Account.Medical_Facility__c != oldMap.get(opps.AccountId).Medical_Facility__c ||
            opps.Pricebook2Id != opps.Account.Default_Price_Book__c ||
            opps.Payor_Code__c != opps.Account.Consumer_Payor_Code__c ||
            opps.Estimated_Cost__c != opps.Account.Estimated_Cost__c ||
            opps.Prescribers__c != opps.Account.Prescribers__c ||
            opps.Medical_Facility__c != opps.Account.Medical_Facility__c ||
            opps.Opp_Territory_AS__c != opps.Account.Territory_AS__c ||
            opps.Opp_Territory_Fax__c != opps.Account.Territory_Fax__c ||
            opps.Opp_Territory_ISR_RingDNA__c != opps.Account.Territory_ISR_RingDNA__c ||
            opps.Opp_Territory_ISR_Email__c != opps.Account.Territory_ISR_Email__c ||
            opps.Opp_Territory_RSS_RingDNA__c != opps.Account.Territory_RSS_RingDNA__c ||
            opps.Opp_Territory_RSS_Email__c != opps.Account.Territory_RSS_Email__c ||
            opps.Opp_Territory_RSS__c != opps.Account.Territory_RSS__c ||
            opps.Opp_Territory_Medicare_RSS__c != opps.Account.Territory_Medicare_RSS__c ||
            opps.Opp_Territory_Medicare_RSS_RingDNA__c != opps.Account.Territory_Medicare_RSS_RingDNA__c ||
            opps.Opp_Territory_Supervisor__c != opps.Account.Territory_Supervisor__c ||
            opps.Opp_Territory_ID__c != opps.Account.Territory_ID_Lookup__c
            //opps.G6_Program__c != opps.Account.G6_Program__c
            )
        )
        {
            Boolean shouldUpdatePricebook = false;
            if(opps.Pricebook2Id != opps.Account.Default_Price_Book__c){
                shouldUpdatePricebook = true;
            }
            
            if(opps.Account.Payor__c != oldMap.get(opps.AccountId).Payor__c){
                opps.Payor__c = opps.Account.Payor__c;
            }
            if(opps.Mobile_Phone__c != oldMap.get(opps.AccountId).PersonContact.MobilePhone)
            {
                opps.Mobile_Phone__c = opps.Account.PersonContact.MobilePhone ;
            }
            // Added by Noy De Goma@CSHERPAS on 11.03.2015
            if(opps.Account.Prescribers__c != oldMap.get(opps.AccountId).Prescribers__c){
                opps.Prescribers__c = opps.Account.Prescribers__c;
            }
            // Added by Noy De Goma@CSHERPAS on 11.03.2015
            if(opps.Account.Medical_Facility__c != oldMap.get(opps.AccountId).Medical_Facility__c){
                opps.Medical_Facility__c = opps.Account.Medical_Facility__c;
            }
            //Added by Jagan on 06.12.2017
            //Altered by Jagan on 10.03.2017 Included logic for customer type change on Account
            if(opps.Count_Of_Oppty_Products__c == 0 || accountsWithMedicareChangesSet.contains(opps.AccountId) || shouldUpdatePricebook){
                system.debug('***********Entering the block to update the Oppty pricebook');
                opps.Pricebook2Id = opps.Account.Default_Price_Book__c;
            }
            //Added by Jagan on 10.03.2017
            if((opps.Count_Of_Oppty_Products__c > 0 && accountsWithMedicareChangesSet.contains(opps.AccountId)) || shouldUpdatePricebook){
                openOpptyWithLineItemsToBeDeletedSet.add(opps.Id);
            }
            //Added by Jagan on 10.04.2017
            if(opps.Payor_Code__c != opps.Account.Consumer_Payor_Code__c){
                opps.Payor_Code__c = opps.Account.Consumer_Payor_Code__c;
            }
            //Added by Jagan on 10.19.2017
            if(opps.Estimated_Cost__c != opps.Account.Estimated_Cost__c){
                opps.Estimated_Cost__c = opps.Account.Estimated_Cost__c;
            }
            //Added by Jagan on 10.20.2017
            //Below lines for Roster updates
            if(opps.Opp_Territory_AS__c != opps.Account.Territory_AS__c){
                opps.Opp_Territory_AS__c = opps.Account.Territory_AS__c;
            }
            if(opps.Opp_Territory_Fax__c != opps.Account.Territory_Fax__c){
                opps.Opp_Territory_Fax__c = opps.Account.Territory_Fax__c;
            }
            if(opps.Opp_Territory_ISR_RingDNA__c != opps.Account.Territory_ISR_RingDNA__c){
                opps.Opp_Territory_ISR_RingDNA__c = opps.Account.Territory_ISR_RingDNA__c;
            }
            if(opps.Opp_Territory_ISR_Email__c != opps.Account.Territory_ISR_Email__c){
                opps.Opp_Territory_ISR_Email__c = opps.Account.Territory_ISR_Email__c;
            }
            if(opps.Opp_Territory_RSS_RingDNA__c != opps.Account.Territory_RSS_RingDNA__c){
                opps.Opp_Territory_RSS_RingDNA__c = opps.Account.Territory_RSS_RingDNA__c;
            }
            if(opps.Opp_Territory_RSS_Email__c != opps.Account.Territory_RSS_Email__c){
                opps.Opp_Territory_RSS_Email__c = opps.Account.Territory_RSS_Email__c;
            }
            if(opps.Opp_Territory_RSS__c != opps.Account.Territory_RSS__c){
                opps.Opp_Territory_RSS__c = opps.Account.Territory_RSS__c;
            }
            if(opps.Opp_Territory_Medicare_RSS__c != opps.Account.Territory_Medicare_RSS__c){
                opps.Opp_Territory_Medicare_RSS__c = opps.Account.Territory_Medicare_RSS__c;
            }
            if(opps.Opp_Territory_Medicare_RSS_RingDNA__c != opps.Account.Territory_Medicare_RSS_RingDNA__c){
                opps.Opp_Territory_Medicare_RSS_RingDNA__c = opps.Account.Territory_Medicare_RSS_RingDNA__c;
            }
            if(opps.Opp_Territory_Supervisor__c != opps.Account.Territory_Supervisor__c){
                opps.Opp_Territory_Supervisor__c = opps.Account.Territory_Supervisor__c;
            }
            if(opps.Opp_Territory_ID__c != opps.Account.Territory_ID_Lookup__c){
                opps.Opp_Territory_ID__c = opps.Account.Territory_ID_Lookup__c;
            }
            
            //BHU 4/3/2018
            //if(opps.G6_Program__c != opps.Account.G6_Program__c){
            //    opps.G6_Program__c = opps.Account.G6_Program__c;
            //}
            
            //Add to the list to be updated
            oppsToBeUpdated.add(opps);
        }
    }
    
    //Added by Jagan on 10.03.2017
    //Removes the Opp products for the Open opportunitied tied to the accounts, which were affected by Medicare customer type logic change
    if(!openOpptyWithLineItemsToBeDeletedSet.isEmpty())
    {
        //  List holds the opportunity line items, which needs to be removed
        List<OpportunityLineItem> oppLineItemsToBeRemoved = new List<OpportunityLineItem>([SELECT Id FROM OpportunityLineItem WHERE OpportunityId IN : openOpptyWithLineItemsToBeDeletedSet]);
        if(!oppLineItemsToBeRemoved.isEmpty())
        {
            try{
                delete oppLineItemsToBeRemoved;
            }catch(Exception e){
                system.debug('***Opp Line Items Deletion Error = ' + e.getMessage());
            }
        }
    }
    
    //Added logic to not update Opportunity if we dont need to
    if(!oppsToBeUpdated.isEmpty())
    {
        //Updates the Opportunity
        try{
            update oppsToBeUpdated;
        }catch(Exception e){
            system.debug('***Opp Update Error = ' + e.getMessage());
        }
    } 
    }   
}