/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WrapMSGwithTime {
    @AuraEnabled
    global String datepreview;
    @AuraEnabled
    global String emojiResourcUrl;
    @AuraEnabled
    global String fieldApiName;
    @AuraEnabled
    global String fileURl_1;
    @AuraEnabled
    global String fileURl_2;
    @AuraEnabled
    global String fileURl_3;
    @AuraEnabled
    global String fileURl_4;
    @AuraEnabled
    global String fileURl_5;
    @AuraEnabled
    global String fileURl_6;
    @AuraEnabled
    global String icon_1;
    @AuraEnabled
    global String icon_1Style;
    @AuraEnabled
    global String icon_2;
    @AuraEnabled
    global String icon_2Style;
    @AuraEnabled
    global String icon_3;
    @AuraEnabled
    global String icon_3Style;
    @AuraEnabled
    global String icon_4;
    @AuraEnabled
    global String icon_4Style;
    @AuraEnabled
    global String icon_5;
    @AuraEnabled
    global String icon_5Style;
    @AuraEnabled
    global String icon_6;
    @AuraEnabled
    global String icon_6Style;
    @AuraEnabled
    global Boolean isSelected;
    @AuraEnabled
    global Map<String,String> mapLookupFieldToRecordName;
    @AuraEnabled
    global Map<String,Map<String,String>> mapMessageIdToMapLookupFieldToLookupFieldRecordName;
    @AuraEnabled
    global String msgTxt;
    @AuraEnabled
    global tdc_tsw__Message__c objMsg;
    @AuraEnabled
    global Boolean showDate;
    @AuraEnabled
    global String strCreatedDate;
    @AuraEnabled
    global String strCreatedTime;
    global WrapMSGwithTime(tdc_tsw__Message__c objMsg, String msgTxt, String emojiResourcUrl) {

    }
}
