/*************************************************************************************************
Apex Class Name : OrderAudit
Version : 0.1
Created Date :  13 Dec, 2018
Function :  This is a server-side class for Order_Audit Component. 
---------------------------------------------------------------------------------------------------
* Developer  LTI                     Date 13 Dec, 2018                           	
**************************************************************************************************/
public class CtrlOrderAudit {
    // Class to Throw Custom exception
    public class applicationException extends Exception {}
    
    // This method Returns the Visibility and Access level indicators through a Wrapper Class
    @auraenabled
    public static wrapperClass getaccessibility(string recid){
        list<RecordType> lstrt = new list<RecordType>();
        set<string> srid = new set<string>();
        wrapperClass returnwrapperClass = new  wrapperClass ();
        returnwrapperClass.hasAccess=false;
        returnwrapperClass.isEditable=false;
        returnwrapperClass.iscomplete=false;
        list<Audit_Tracker__c> lstAt = new list<Audit_Tracker__c> ();
        /*   
        lstrt =  [Select Id
                 from RecordType 
                where developername in ('DE_Sales_Order_Read_Only','DE_Sales_Order')
              	];
        for(recordtype rt: lstrt){
            string recordid = String.valueOf(rt.Id).substring(0, 15);
            srid.add(recordid);
        }
        if(srid.size() > 0){
		*/
            list<order> lstorder= [select id,status,shipping_hold_status__c 
                    from order
                   where id=:recid
                  	 //and recordtypeid != null            ////////// **************** Highly kludged code. Need to be changed. These are validated at the Component Visibility Level itself. So, no need to worry!
                     //and recordtypeid in :srid
                    // and type='DE STANDARD'
                     //and is_cash_order__c = false
                                  ]; 
            if(lstorder.size() > 0){
                returnwrapperClass.hasAccess=true;
                returnwrapperClass.Shipholdstatus=lstorder[0].shipping_hold_status__c;
                if(lstorder[0].shipping_hold_status__c == 'completed'){
               	   returnwrapperClass.iscomplete=true;   			      
                }
                Boolean canAudit = (FeatureManagement.checkPermission('DE_Can_Audit_Orders') || FeatureManagement.checkPermission('Can_Audit_Orders'));
                if(lstorder[0].status == 'shipping hold' && canAudit){
                   returnwrapperClass.isEditable=true;
                }    
            }
         //}
        return returnwrapperClass;
    }
    // This method Returns the Related Audit records
    @auraenabled
    public static Map<string,list<Audit_Tracker__c>> inithelpermethod(string recid){
        
        list<Audit_Tracker__c> lstAt = new list<Audit_Tracker__c> ();
        Map<string,list<Audit_Tracker__c>> mresult = new Map<string,list<Audit_Tracker__c>>();
        /*
        list<RecordType> lstrt = new list<RecordType>();
        set<string> srid = new set<string>();
        lstrt =  [Select Id
                 from RecordType 
                where developername in ('DE_Sales_Order_Read_Only','DE_Sales_Order')
              	];
        for(recordtype rt: lstrt){
            string recordid = String.valueOf(rt.Id).substring(0, 15);
            srid.add(recordid);
        }
		*/
       // if(srid.size() > 0){
            try{
                 lstAt= [select id, Audit_Field_Name__c,Field_Verified__c,Object_id__c 
                                             from Audit_Tracker__c
                                            where Object_id__c != null
                                              and Object_id__c=:recid
                                              //and Object_id__r.recordtypeid != null
                                              //and Object_id__r.recordtypeid in :srid
                                              //and Object_id__r.type='DE STANDARD'
                                              //and Object_id__r.is_cash_order__c = false
                        ];            
                if(lstAt.size() > 0){
                    mresult.put('Success',lstAt);
                } else {
                    //throw new applicationException('No Audit Records');
                }   
            } catch (Exception e){
                mresult.put('Error-'+e.getMessage(),lstAt);
				// Throw an AuraHandledException
				throw new AuraHandledException('Error :  ' + e.getMessage());    
			}
       // }    
        return mresult;
    }
    // This Method Saves the updated Audit Records
    @auraenabled
    public static string saveaudit(list<Audit_Tracker__c> auditrecs){
        String showerror = 'success';
        try{
            update auditrecs;
        } catch (Exception e){
            showerror = 'Error-'+e.getMessage();
			throw new AuraHandledException('Error :  ' + e.getMessage()); 
        }
        return showerror;
    }
    // This Method Submits the updated Audit Records. If all records are Audited, Order is Activated.
    // Else, Status is changed to 'Draft'
    @auraenabled
    public static string submitaudit(string  recordId,list<Audit_Tracker__c> lstaudit){
        //if (!ClsOrderHandlerStatic.runSubmitOrder()) return 'Submit is already being processed';
        String showerror = 'success';
        order o1 = new order();
        try{
            boolean auditincomplete = false;
            o1= [select id,status,ownerid,recordtypeid,shipping_hold_status__c, Payment_Id__c, Settlement_Id__c, Payment_Terms__c, recordType.DeveloperName
                   from order 
                  where id =:recordId];
            for(Audit_Tracker__c at: lstaudit) {
                if(at.Field_Verified__c == false){
                    auditincomplete = true;
                }
            }
            if(auditincomplete == true){
               // Verification Fail 
               // Update Order status / recordtype 
               string result= CtrlOrderAudit.updateorder('draft',o1);
               update lstaudit; 
               // create a task for order owner to correct information
               task t = new task();
               t.subject = 'Please update Order Information and then, Change the status to Shipping Hold';
               t.ownerid=o1.ownerid;
               t.whatid=o1.id;
               insert t; 
            } else {
                if (o1.Payment_Terms__c == 'net0') { // Credit card is to be used
                    if (String.isNotBlank(o1.Payment_Id__c)){
                        String result = ClsOrderEntryHandler.CCsettlePayment(o1.id);
                        if (result.startsWith('Error')) {
                            String recName = o1.RecordType.DeveloperName.removeEndIgnoreCase('_Read_Only');
                            Id recId = [Select Id from RecordType where SobjectType='Order' AND isActive=true AND DeveloperName = :recName].Id;
                            Order updOrd = new Order(Id = o1.Id, Status = 'Draft', RecordTypeID = recId, Settlement_Error__c = result, Payment_Id__c = null);
                            update updOrd;
                            return result;
                        }
                    } else {
                        String result = ClsOrderEntryHandler.captureCCpayment(o1.id);
                    	if (result.startsWith('Error')) {
                            String recName = o1.RecordType.DeveloperName.removeEndIgnoreCase('_Read_Only');
                            Id recId = [Select Id from RecordType where SobjectType='Order' AND isActive=true AND DeveloperName = :recName].Id;
                            Order updOrd = new Order(Id = o1.Id, Status = 'Draft', RecordTypeID = recId, Settlement_Error__c = result, Payment_Id__c = null);
                            update updOrd;
                            return result;
                        }
                    }
                }
                //Look for payment terms and see if Credit Card needs to be charged
               // verification success 
               // Update Order status / recordtype 
               // Adding the following so that Orders get published.
               ClsOrderHandlerStatic.executeOrderEventTriggerv11 = true;
               o1.Shipping_Hold_Status__c='completed';
               string result= CtrlOrderAudit.updateorder('activated',o1);
               update lstaudit;
            }
        } catch(DMLException e){
            showerror = 'Error-'+e.getMessage();
			throw new AuraHandledException('Error :  ' + e.getMessage()); 
        }   
        return showerror;
    }
    // This Method Updates the Order Status and Record Type
    @auraenabled
    public static string updateorder(string status,order o1){
        string result='';
        String recName = o1.RecordType.DeveloperName.removeEndIgnoreCase('_Read_Only');
        Id recId = [Select Id from RecordType where SobjectType='Order' AND isActive=true AND DeveloperName = :recName].Id;
        /*
    	list<RecordType> rt = new list<recordtype>();
        rt =  [Select Id
                    from RecordType 
                   where developername = 'DE_Sales_Order'];
        if(rt.size() > 0){
		*/
            try{
                o1.Status = status;
                if (recId != null) o1.RecordTypeId = recId;
                update o1;
                result= 'success';
            }  catch(Exception e){
				if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) 
                {
                    result = e.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
					Pattern pattern = Pattern.compile('[^a-z A-Z]');
					Matcher matcher = pattern.matcher(result);
					result = matcher.replaceAll('');
                }
				AuraHandledException ex = new AuraHandledException(result);
				ex.setMessage(result);
				throw ex;
            }   
        return result;   
    }
    // This is a Wrapper Class for Visibility and Access Indicators
    public class wrapperClass{
        @AuraEnabled public Boolean hasAccess{get;set;}
        @AuraEnabled public Boolean isEditable{get;set;}
        @AuraEnabled public Boolean iscomplete{get;set;}
        @AuraEnabled public string Shipholdstatus{get;set;}
    }
}