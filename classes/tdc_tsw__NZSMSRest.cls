/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/NZIncoming/*')
global class NZSMSRest {
    global NZSMSRest() {

    }
    global static void doForIncoming() {

    }
    @HttpGet
    global static void doGet() {

    }
    @HttpPost
    global static void doPost() {

    }
}
