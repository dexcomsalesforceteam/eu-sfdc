({
	 doInit : function(component, event, helper) {
         // Loads Related Audit Records 
         helper.inithelper(component);
     },
	 saveauditbtn : function(cmp,event,helper) {
       // Save the Updated Audit Records  
       helper.saveaudithelper(cmp);
     },
     submitauditbtn : function(cmp,event,helper) {
       // Submits the Audit Records  
       helper.submitaudithelper(cmp);
     },
    selectchange : function(component, event, helper) {
         console.log(component.get("v.auditlist"));
         var s=event.getSource();
         var v=s.get("v.value");
         console.log(v);
     },
})