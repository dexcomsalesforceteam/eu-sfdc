({
	inithelper : function(cmp) {
        if(!$A.util.isUndefined(cmp)){
            var that = this;
        	that.queryaccess(cmp);            
        }
        
         
    },
    saveaudithelper : function(cmp) {
		// Call Apex method to Save the updated Audit Records
        var that = this;
        var action = cmp.get("c.saveaudit");
        action.setParams({ 
            auditrecs : cmp.get("v.auditlist")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                 	var result=response.getReturnValue();
                    if(result.indexOf('Error') > -1){
            		    that.showerror(cmp,result.split('-')[1]);
                    } else {
                        that.showsuccess(cmp,'Record Saved Successfully');
                        that.refreshcomp(cmp);
                        
                    }
            }
            else if (state === "INCOMPLETE") {
                Console.log('Server Call status Incomplete ');
            }
            else if (state === "ERROR") {
                 that.showerror(cmp,response.getError()[0].message);
            }
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
	},
	submitaudithelper : function(cmp) {
		// call the Apex Method to Submit the Audit Records
        var that = this;

        var action = cmp.get("c.submitaudit");
        action.setParams({ 
            recordId : cmp.get("v.recordId"),
            lstaudit : cmp.get("v.auditlist")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
					var result=response.getReturnValue();
                    if(result.indexOf('Error') > -1){
            		    that.showerror(cmp,result.split('-')[1]);
                    } else {
                        that.showsuccess(cmp,'Record Submitted Successfully');
                        that.refreshcomp(cmp);
                    }
            }
            else if (state === "INCOMPLETE") {
                     Console.log('Server Call status Incomplete ');
            }
            else if (state === "ERROR") {
               that.showerror(cmp,response.getError()[0].message);
            }
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
	},
    showsuccess : function(cmp,msg){
        // This Method Shows the Success Message
  		    			var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Action Info',
                            message: msg,
                            duration:'5000',
                            type: 'success',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
    },
    showerror : function(cmp,msg){
        // This Method Shows the Error Message
  		    			var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error Message',
                            message: msg,
                            duration:'5000',
                            type: 'error',
                            mode: 'dismissible'
                        });
                        toastEvent.fire();
    },
    refreshcomp : function(cmp){
        // This Method refreshes the current component
        $A.get('e.force:refreshView').fire();
    },
    queryaccess : function(cmp){
        // Call the Apex method to get Read/Edit Access and 
        // also Visibility for related Audit Records
        var that = this;
  		var action = cmp.get("c.getaccessibility");
        action.setParams({ 
            recid : cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.wrapperList",response.getReturnValue());
                var shipstatus=cmp.get("v.wrapperList.Shipholdstatus");
                var statusdiv = cmp.find('statusbannerid');
                if( shipstatus=="Not Started"){
                    $A.util.addClass(statusdiv,'color1');
                } else if( shipstatus=="In Progress"){
                    $A.util.addClass(statusdiv,'color2');
                } else if( shipstatus=="Completed"){
                    $A.util.addClass(statusdiv,'color3');
                }
                if(cmp.get("v.wrapperList").hasAccess){
                    that.getauditrecords(cmp);    
                }
                if(cmp.get("v.wrapperList").iscomplete){
                   cmp.set("v.isComplete",cmp.get("v.wrapperList").iscomplete);
                }
                if(!cmp.get("v.wrapperList").isEditable){
                    var completediv = cmp.find('maindiv');
                    $A.util.addClass(completediv,'readonlydiv');
                } 
            }
            else if (state === "ERROR") {
               that.showerror(cmp,response.getError()[0].message);
            }
        });
  		// $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);      
    },
    getauditrecords : function(cmp){
        // call the Apex Method to Get related Audit Records
        var action = cmp.get("c.inithelpermethod");
        action.setParams({ 
            recid : cmp.get("v.recordId")
        });
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                for (var key in result){
                    if(key.indexOf('Error') > -1){
            		    that.showerror(cmp,key.split('-')[1]);
                    } else {
                        console.log(result[key]);
                        cmp.set("v.auditlist",result[key]); 
                    }
            	}    	
            }
            else if (state === "INCOMPLETE") {
                     Console.log('Server Call status Incomplete ');
            }
            else if (state === "ERROR") {
                 that.showerror(cmp,response.getError()[0].message);
            }
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    }
})