<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_to_record_Id</fullName>
        <description>Set the System of Origin Id to Record Id</description>
        <field>System_Of_Origin_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set to record Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_System_of_Origin</fullName>
        <description>Set it to crm</description>
        <field>System_Of_Origin__c</field>
        <formula>&apos;crm&apos;</formula>
        <name>Update System of Origin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Address_To_Oracle_1</fullName>
        <apiVersion>41.0</apiVersion>
        <description>Outbound message to send Account updates
12/30/19 - Removed PROD URL - https://eu.connect.boomi.com/ws/soap/upatecustomer;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPLklQVFNCSjphYWM3Y2UzNi00Yzg0LTQ3ZjMtOWRkOS1kZTlhMmU1M2M1OTY=</description>
        <endpointUrl>https://dummyurl.com</endpointUrl>
        <fields>Account_Party_Id__c</fields>
        <fields>Account__c</fields>
        <fields>Active_Start_Date__c</fields>
        <fields>Address_Line_1__c</fields>
        <fields>Address_Line_2__c</fields>
        <fields>Address_Line_3__c</fields>
        <fields>City__c</fields>
        <fields>Country__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>Id</fields>
        <fields>Oracle_Cust_Site_Id__c</fields>
        <fields>Oracle_Customer_Id__c</fields>
        <fields>Oracle_Location_Id__c</fields>
        <fields>Oracle_Party_Site_Id__c</fields>
        <fields>Postal_Code__c</fields>
        <fields>Primary_Flag__c</fields>
        <fields>Record_Type_Name__c</fields>
        <fields>State__c</fields>
        <fields>Type__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.eu</integrationUser>
        <name>Send Address To Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>All Address Default SOO</fullName>
        <actions>
            <name>Update_System_of_Origin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the System of Origin to crm if the Address record is created there</description>
        <formula>ISBLANK( System_Of_Origin__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>All Address Default SOO ID</fullName>
        <actions>
            <name>Set_to_record_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the System of Origin ID to Record Id if the Address is record is created there</description>
        <formula>ISBLANK(  System_Of_Origin_Id__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DE Send Address To Oracle</fullName>
        <actions>
            <name>Send_Address_To_Oracle_1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Jagan 08/11/2017 - Rule sends address record to Oracle upon create or update provided the associated account has Party Id associated.</description>
        <formula>AND( BEGINS(RecordType.DeveloperName, &apos;DE&apos;), NOT(ISBLANK(Account__r.Party_Id__c)), OR( OR( ISNEW(), ISCHANGED(Address_Line_1__c), ISCHANGED(Address_Line_2__c), ISCHANGED(Address_Line_3__c), ISCHANGED(City__c), ISCHANGED(State__c), ISCHANGED(Postal_Code__c), ISCHANGED(Primary_Flag__c), ISCHANGED(Type__c) ), AND( ISCHANGED(Send_To_Oracle__c), Send_To_Oracle__c = true ) )	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Address To Oracle</fullName>
        <actions>
            <name>Send_Address_To_Oracle_1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Jagan 08/11/2017 - Rule sends address record to Oracle upon create or update provided the associated account has Party Id associated.</description>
        <formula>AND( 	NOT(ISBLANK(Account__r.Party_Id__c)), 	OR( 		OR( 			ISNEW(), 			ISCHANGED(Address_Line_1__c), 			ISCHANGED(Address_Line_2__c), 			ISCHANGED(Address_Line_3__c), 			ISCHANGED(City__c), 			ISCHANGED(State__c), 			ISCHANGED(Postal_Code__c), 			ISCHANGED(Primary_Flag__c), 			ISCHANGED(Type__c) 		), 		AND(  			ISCHANGED(Send_To_Oracle__c),  			Send_To_Oracle__c = true  		) 	)	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
