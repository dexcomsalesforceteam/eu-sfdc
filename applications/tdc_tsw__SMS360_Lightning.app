<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>tdc_tsw__X360_SMS</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <formFactors>Large</formFactors>
    <label>360 SMS</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Lead</tab>
    <tab>tdc_tsw__Message__c</tab>
    <tab>tdc_tsw__Message_Template__c</tab>
    <tab>tdc_tsw__SMS_Setup</tab>
    <tab>tdc_tsw__SMS_From_Reports</tab>
    <tab>tdc_tsw__SMS_App_Help</tab>
    <tab>tdc_tsw__SMS_To_User</tab>
    <tab>tdc_tsw__Create_View_Survey</tab>
    <tab>tdc_tsw__SMS_Unsubscribes__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>tdc_tsw__X360_SMS_Lightning_UtilityBar</utilityBar>
</CustomApplication>
