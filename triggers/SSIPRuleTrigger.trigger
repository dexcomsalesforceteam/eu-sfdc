trigger SSIPRuleTrigger on SSIP_Rule__c (after insert, after update) {
    
    if(Trigger.isAfter && Trigger.isInsert){
        new SSIPRuleTriggerHandler().onAfterInsert(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isUpdate){
        new SSIPRuleTriggerHandler().onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}