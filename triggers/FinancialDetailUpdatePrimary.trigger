trigger FinancialDetailUpdatePrimary on Finance_Detail__c (after update) {
    if(Trigger.isAfter && Trigger.isUpdate) {
        List<Finance_Detail__c> lstFDUpd= new List<Finance_Detail__c>();
        for(Finance_Detail__c fd: Trigger.New){            
            if(fd.Primary__c && (Trigger.OldMap.get(fd.Id).Primary__c !=fd.Primary__c)){
                lstFDUpd.Add(fd);    
            }
        }
        if(!lstFDUpd.isEmpty()){
            clsJobMarkCardsNonPrimary jobUpdateCC = new clsJobMarkCardsNonPrimary(lstFDUpd);                
            ID jobID = System.enqueueJob(jobUpdateCC);
        }
        
    }
}