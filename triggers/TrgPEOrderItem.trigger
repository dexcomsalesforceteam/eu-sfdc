trigger TrgPEOrderItem on CRM_OrderItem_Event__e (after insert) {
    for(CRM_OrderItem_Event__e  e: trigger.new) system.debug('Received OrderItem Event: ' + e);
    PEClsHandleOrderItemEvents.handleIncomingEvents(trigger.new);
}